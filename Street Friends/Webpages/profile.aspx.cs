﻿using System;
using Model;
using BusinessLogic;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace Street_Friends.Webpages
{
    public partial class profile : System.Web.UI.Page
    {
        User myUser;
        public bool isPasswordSame = true;
        public bool isPasswordUpdated;
        public bool isDeleted = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            lb_Name.Text = Session["FullName"].ToString();

            myUser = new User();
            UserLogic userLogic = new UserLogic();
            myUser = userLogic.GetUserDetailsByIDBL(int.Parse(Session["UserId"].ToString()));

            lb_Email.Text = myUser.email;

            showYourExperience();
            showFavoriteExperience();
        }

        protected void btn_Update_Click(object sender, EventArgs e)
        {
            if (myUser.password == tb_OldPassword.Text)
            {
                UserLogic userLogic = new UserLogic();
                int result = userLogic.UpdatePasswordBL(int.Parse(Session["UserId"].ToString()),tb_NewPassword.Text);

                isPasswordUpdated = result == 1 ? true : false; 
            }
            else
            {
                isPasswordSame = false;
            }
        }

        public void showYourExperience()
        {
            ExperienceLogic experience = new ExperienceLogic();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            adapter = experience.ShowYourExperienceBL(int.Parse(Session["UserId"].ToString()));
            adapter.Fill(dataSet, "Experience");
            rp_Experience.DataSource = dataSet.Tables["Experience"];
            rp_Experience.DataBind();
        }

        public void showFavoriteExperience()
        {
            ExperienceLogic experience = new ExperienceLogic();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            adapter = experience.ShowFavoriteExperienceBL(int.Parse(Session["UserId"].ToString()));
            adapter.Fill(dataSet, "FavoriteExperience");
            rp_Favorite.DataSource = dataSet.Tables["FavoriteExperience"];
            rp_Favorite.DataBind();
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            Button button = (sender as Button);
            int experienceId = int.Parse(button.CommandArgument);

            ExperienceLogic experienceLogic = new ExperienceLogic();
            int result = experienceLogic.DeleteExperienceBL(experienceId);

            isDeleted = result == 1 ? true : false;

            showYourExperience();
        }

        protected void rp_Experience_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (rp_Experience.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lbl_Message");
                    lblFooter.Visible = true;
                }
            }
        }
        
        
        protected void rp_Favorite_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (rp_Favorite.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lbl_Message");
                    lblFooter.Visible = true;
                }
            }
        }
    }
}