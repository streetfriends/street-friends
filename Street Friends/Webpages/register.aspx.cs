﻿using System;
using Model;
using BusinessLogic;
using System.Net;
using System.Collections.Specialized;

namespace Street_Friends.Webpages
{
    public partial class register : System.Web.UI.Page
    {
        public bool isRegistered = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.Parse(Session["UserId"].ToString()) == 0 && Session["FullName"] == null)
            {
                hl_One.Text = "Login";
                hl_One.NavigateUrl = "login.aspx";

                hl_Two.Text = "Register";
                hl_Two.NavigateUrl = "register.aspx";
            }
            else
            {
                lb_Name.Text = Session["FullName"].ToString();
                hl_One.Text = "Profile";
                hl_One.NavigateUrl = "profile.aspx";

                hl_Two.Text = "Logout";
                hl_Two.NavigateUrl = "logout.aspx";
            }
        }

        protected void btn_Register_Click(object sender, EventArgs e)
        {
            UserLogic userLogic = new UserLogic();
            User myUser = new User();

            myUser.fullname = tb_FullName.Text;
            myUser.email = tb_Email.Text;
            myUser.password = tb_Password.Text;

            if (userLogic.RegisterUserBL(myUser))
                isRegistered = true;
            else
                isRegistered = false;

            tb_FullName.Text = string.Empty;
            tb_Email.Text = string.Empty;
            tb_Password.Text = string.Empty;
        }
    }
}