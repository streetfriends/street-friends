﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="stats.aspx.cs" Inherits="Street_Friends.Webpages.stats" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Homelessness Stats</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="../Assets/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <link href="../Assets/css/colorbox.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {
            $(".stat").colorbox({ iframe: true, innerWidth: "75%", innerHeight: "88%" });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                            <div class="name">
                                <h3 class="title">Homeless Facts</h3>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <img src="../Assets/img/vic_stats.jpg" width="1160" />
                        <br />
                    </div>
                    <div class="row">
                        <h3>National-based infographics</h3>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/Homelessness_in_Australia_-_updated_Jan_2014.pdf"><strong>Homelessness in Australia infographic</strong></a></h4>
                        <h3>State-based infographics</h3>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/ACT_-_updated_Jan_2014.pdf"><strong>Homelessness in the ACT</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/NSW_-_updated_Jan_2014.pdf"><strong>Homelessness in New South Wales</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/NT_-_updated_Jan_2014.pdf"><strong>Homelessness in the NT</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/QLD_-_updated_Jan_2014.pdf"><strong>Homelessness in Queensland</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/SA_-_updated_Jan_2014.pdf"><strong>Homelessness in South Australia</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/TAS_-_updated_Jan_2014.pdf"><strong>Homelessness in Tasmania</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/Victoria_-_updated_Jan_2014.pdf"><strong>Homelessness in Victoria</strong></a></h4>
                        <h4><a class='stat' href="http://www.homelessnessaustralia.org.au/sites/homelessnessaus/files/2017-07/WA_-_updated_Jan_2014.pdf"><strong>Homelessness in WA</strong></a></h4>
                        <br />
                        <p><strong>The statistics in these infographics are from:</strong></p>
                        <p>ABS, 2012, Census of Population and Housing: Estimating Homelessness 2011</p>
                        <p>AIHW, 2013, Specialist Homelessness Services Data Collection 2012-13</p>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
