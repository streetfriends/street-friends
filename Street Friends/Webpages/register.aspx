﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Street_Friends.Webpages.register" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Register at Street Friends</title>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="../Assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../Assets/css/material-kit.css" rel="stylesheet" />
</head>
<body class="signup-page">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="landing-page.aspx">Street Friends</a>
            </div>

            <div class="collapse navbar-collapse" id="navigation-example">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="landing-page.aspx">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Understanding Homeless
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="homeless-list.aspx">Homeless Stories</a></li>
                            <li><a href="stats.aspx">Homeless Facts</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Helping Homeless
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="volunteer-list.aspx">Find Volunteering Opportunities</a></li>
                            <li><a href="support-service.aspx">Support Service for Homeless</a></li>
                            <li><a href="employ-service.aspx">Employment Service for Homeless</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="experience.aspx">Sharing Volunteering Experience</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Label ID="lb_Name" runat="server" Text="User"></asp:Label>
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><asp:HyperLink ID="hl_One" runat="server"></asp:HyperLink></li>
                            <li><asp:HyperLink ID="hl_Two" runat="server"></asp:HyperLink></li>
                        </ul>
                    </li>        
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper">
        <div class="header header-filter" style="background-image: url('../Assets/img/city.jpg'); background-size: cover; background-position: top center;">
            <div class="container">
                <div class="row">
                    <% if(isRegistered) { %>
                    <div class="alert alert-success" style="margin-top:80px; margin-bottom:-80px;">
                        <div class="container-fluid">
	                        <div class="alert-icon">
		                        <i class="material-icons">check</i>
	                        </div>
	                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                        </button>
                            <b>Message:</b> Yuhuuu! <a href="login.aspx"> <strong>Click Here</strong></a> to Login. You've been registered Sucessfully.
                        </div>
                    </div>
                    <%}%>
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="card card-signup">
                            <form runat="server" class="form">
                                <div class="header header-primary text-center">
                                    <h4>Sign Up</h4>
                                </div>
                                <div class="content">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <asp:RequiredFieldValidator ID="rf_FullName" runat="server" ErrorMessage="Enter Full Name" ControlToValidate="tb_FullName" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="tb_FullName" runat="server" CssClass="form-control" placeholder="Full Name..."></asp:TextBox>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">email</i>
                                        </span>
                                        <asp:RequiredFieldValidator ID="rf_Email" runat="server" ErrorMessage="Enter Email Address" ControlToValidate="tb_Email" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="tb_Email" TextMode="Email" runat="server" CssClass="form-control" placeholder="Email..."></asp:TextBox>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <asp:RequiredFieldValidator ID="rf_Password" runat="server" ErrorMessage="Enter Password" ControlToValidate="tb_Password" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="tb_Password" runat="server" CssClass="form-control" TextMode="Password" placeholder="Password..."></asp:TextBox>
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        
                                        <asp:CompareValidator ID="cv_Match" ControlToValidate="tb_Confirm" ControlToCompare="tb_Password" runat="server" ErrorMessage="Passwords do not match" Display="Dynamic" ForeColor="Red" Operator="Equal" Type="String"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="rf_Confirm" runat="server" ErrorMessage="Please Re-Enter Password" ControlToValidate="tb_Confirm" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="tb_Confirm" runat="server" CssClass="form-control" TextMode="Password" placeholder="Confirm Password..."></asp:TextBox>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <asp:Button ID="btn_Register" CssClass="btn btn-success" runat="server" Text="Register" OnClick="btn_Register_Click"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">
                        <ul>
                            <li>
                                <a href="team.aspx">Team Inspire</a>
                            </li>
                            <li>
                                <a href="feedback.aspx">Feedback</a>
                            </li>
                        </ul>
                    </nav>
                    <div class="copyright pull-right">
                        &copy; 2017 - Made by Team Inspire
                    </div>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../Assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../Assets/js/material.min.js"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="../Assets/js/material-kit.js" type="text/javascript"></script>
</html>
