﻿using System;
using Model;
using BusinessLogic;

namespace Street_Friends.Webpages
{
    public partial class employ_service_detail : System.Web.UI.Page
    {
        public string name;
        public string address;
        public float latitude;
        public float longitude;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            /*if (!IsPostBack)
            {
                string sitecode = Request.QueryString["sitecode"].ToString();
                Employment employment = new Employment();

                EmploymentLogic employmentLogic = new EmploymentLogic();
                employment = employmentLogic.GetEmploymentServiceBySiteCodeBL(sitecode);

                lb_Name.Text = employment.siteName;
                lb_Address.Text = employment.siteAddress;

                if (employment.phoneNo.ToString() != "")
                    lb_Phone.Text = employment.phoneNo.ToString();

                if (employment.email != "")
                {
                    hl_Email.Text = employment.email;
                    hl_Email.NavigateUrl = "mailto:" + employment.email;
                }

                if (employment.url != "")
                {
                    hl_URL.Text = employment.url;

                    if (!employment.url.StartsWith("http://"))
                        hl_URL.NavigateUrl = "http://" + employment.url;
                }

                name = employment.siteName;
                address = employment.siteAddress;
                latitude = float.Parse(employment.latitude);
                longitude = float.Parse(employment.longitude);
            }*/
        }

        protected void btn_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("employ-service.aspx");
        }
    }
}