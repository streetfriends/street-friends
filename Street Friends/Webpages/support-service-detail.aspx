﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="support-service-detail.aspx.cs" Inherits="Street_Friends.Webpages.support_service_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Support Service Detail</title>
    <link href="../Assets/css/googlemap.css" rel="stylesheet" />
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>

    <%if (isLocationAvailable) { %>
        <script type="text/javascript">
            function initMap() {
                var uluru = { lat: <%=latitude%>, lng: <%=longitude%>};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: uluru
                });

                var contentString = "<h4><strong><%=name%></strong></h4><h5><strong>Address:</strong> <%=address%></h5>"

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map
                });

                marker.addListener('click', function () {
                    infowindow.open(map, marker);
                });
            }
        </script>
    
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyQtlz9311EcdiMUeQWNl5Tk5RhkDrg5M&callback=initMap" type="text/javascript"></script>
    <% } %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="profile">
                        <div class="name">
                            <h3 class="title">
                                <asp:Label ID="lb_Name" runat="server"></asp:Label>
                                <div class="pull-right">
                                    <asp:Button ID="btn_Back" runat="server" Text="Go Back" CssClass="btn btn-info btn-round" OnClick="btn_Back_Click" />
                                </div>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Address:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Address" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <label>Phone:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Phone" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Email:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:HyperLink ID="hl_Email" runat="server" Text=""></asp:HyperLink>
                                </div>
                                <div class="col-md-2">
                                    <label>Website:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:HyperLink ID="hl_URL" runat="server" Text=""></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Tram Route:</label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Tram" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:Label ID="lb_BusText" runat="server"><label>Bus Route:</label></asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Bus" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <asp:Label ID="lb_StationText" runat="server"><label>Nearest Station:</label></asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Station" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="col-md-2">
                                    <asp:Label ID="lb_CostText" runat="server"><label>Cost:</label></asp:Label>
                                </div>
                                <div class="col-md-4">
                                    <asp:Label ID="lb_Cost" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <label>Categories:</label>
                                </div>
                                <div class="col-md-10">
                                    <asp:Label ID="lb_Categories" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr style="background-color:lightgray">
                                            <%if (dayName == "Monday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Monday</strong></td>
                                            <%} else { %>
                                                <td><strong>Monday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Tuesday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Tuesday</strong></td>
                                            <%} else { %>
                                                <td><strong>Tuesday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Wednesday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Wednesday</strong></td>
                                            <%} else { %>
                                                <td><strong>Wednesday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Thursday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Thursday</strong></td>
                                            <%} else { %>
                                                <td><strong>Thursday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Friday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Friday</strong></td>
                                            <%} else { %>
                                                <td><strong>Friday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Saturday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Saturday</strong></td>
                                            <%} else { %>
                                                <td><strong>Saturday</strong></td>
                                            <%} %>
                                            <%if (dayName == "Sunday") { %>
                                                <td style="background-color:cornflowerblue"><strong>Sunday</strong></td>
                                            <%} else { %>
                                                <td><strong>Sunday</strong></td>
                                            <%} %>
                                            <td><strong>Public Holiday</strong></td>
                                        </tr>
                                        <tr>
                                            <td><asp:Label ID="lb_Monday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Tuesday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Wednesday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Thursday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Friday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Saturday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_Sunday" runat="server" Text="N/A"></asp:Label></td>
                                            <td><asp:Label ID="lb_PublicHoliday" runat="server" Text="N/A"></asp:Label></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <%if (isLocationAvailable)
                                                    { %>
                                    <div id="map"></div>
                                <% } %>
                            </div>
                        </div>
                        <% if (isTwitterAvaiable) { %>
                            <div class="col-md-4">
                                <a class="twitter-timeline" data-tweet-limit="5" href="https://twitter.com/<%=twitter%>">Tweets by <%=twitter%></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                            </div>
                        <% } %>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
