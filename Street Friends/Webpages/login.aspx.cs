﻿using System;
using BusinessLogic;

namespace Street_Friends.Webpages
{
    public partial class login : System.Web.UI.Page
    {
        public int isAuthenticated = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.Parse(Session["UserId"].ToString()) == 0 && Session["FullName"] == null)
            {
                hl_One.Text = "Login";
                hl_One.NavigateUrl = "login.aspx";

                hl_Two.Text = "Register";
                hl_Two.NavigateUrl = "register.aspx";
            }
            else
            {
                lb_Name.Text = Session["FullName"].ToString();
                hl_One.Text = "Profile";
                hl_One.NavigateUrl = "profile.aspx";

                hl_Two.Text = "Logout";
                hl_Two.NavigateUrl = "logout.aspx";
            }
        }

        protected void btn_Login_Click(object sender, EventArgs e)
        {
            UserLogic userLogic = new UserLogic();
            String[] Info = userLogic.AuthenticateBL(tb_Email.Text, tb_Password.Text);

            if (Info[0] != "" && Info[1] != "")
            {
                Session["UserId"] = int.Parse(Info[0]);
                Session["FullName"] = Info[1];
                Response.Redirect("landing-page.aspx");
            }
            else
                isAuthenticated = -1;
        }
    }
}