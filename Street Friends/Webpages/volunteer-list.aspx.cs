﻿using System;
using BusinessLogic;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace Street_Friends.Webpages
{
    public partial class volunteer_list : System.Web.UI.Page
    {
        public int isLogin = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.Parse(Session["UserId"].ToString()) == 0 && Session["FullName"] == null)
                isLogin = 0;
            else
                isLogin = 1;

            if (!IsPostBack)
            {
                getAllVolunteerCenters();
            }
        }

        public void getAllVolunteerCenters()
        {
            VolunteerCenterLogic volunteerCenter = new VolunteerCenterLogic();
            ddl_PostCode.DataSource = volunteerCenter.GetAllPostCodeBL();
            ddl_PostCode.DataBind();

            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            Tuple <SqlDataAdapter, DataTable> allVolunteerCenter = volunteerCenter.GetAllVolunteerCenterBL(int.Parse(Session["UserId"].ToString()));

            adapter = allVolunteerCenter.Item1;
            adapter.Fill(dataSet, "VolunteerCenter");
            rp_Volunteer.DataSource = dataSet.Tables["VolunteerCenter"];
            rp_Volunteer.DataBind();

            rp_Markers.DataSource = allVolunteerCenter.Item2;
            rp_Markers.DataBind();
        }

        protected void btn_Search_Click(object sender, EventArgs e)
        {
            VolunteerCenterLogic volunteerCenter = new VolunteerCenterLogic();
            Tuple<SqlDataAdapter, DataTable> allVolunteerCenter;

            List<string> selectedCategory = new List<string>();

            if (cb_All.Checked && ddl_PostCode.SelectedItem.Text == "All")
            {
                allVolunteerCenter = volunteerCenter.GetAllVolunteerCenterBL(int.Parse(Session["UserId"].ToString()));
            }
            else
            {
                if (!cb_All.Checked)
                {
                    if (cb_Accomodation.Checked)
                    {
                        selectedCategory.Add("Accomodation");
                    }

                    if (cb_Health.Checked)
                    {
                        selectedCategory.Add("Health");
                    }

                    if (cb_Financial.Checked)
                    {
                        selectedCategory.Add("Financial");
                    }

                    if (cb_Children.Checked)
                    {
                        selectedCategory.Add("Children");
                    }

                    if (cb_Women.Checked)
                    {
                        selectedCategory.Add("Women");
                    }

                    if (cb_Legal.Checked)
                    {
                        selectedCategory.Add("Legal");
                    }

                    if (cb_Drugs.Checked)
                    {
                        selectedCategory.Add("Alcohol / Drugs");
                    }

                    if (cb_Training.Checked)
                    {
                        selectedCategory.Add("Training");
                    }

                    if (cb_Support.Checked)
                    {
                        selectedCategory.Add("Support");
                    }

                    if (cb_Food.Checked)
                    {
                        selectedCategory.Add("Food");
                    }

                    if (cb_Cloths.Checked)
                    {
                        selectedCategory.Add("Cloths");
                    }

                    if (cb_Employment.Checked)
                    {
                        selectedCategory.Add("Employment");
                    }

                    if (ddl_PostCode.SelectedItem.Text != "All")
                        allVolunteerCenter = volunteerCenter.GetVolunteerByCategoryAndPostCodeBL(int.Parse(Session["UserId"].ToString()), selectedCategory, ddl_PostCode.SelectedItem.Text);
                    else
                        allVolunteerCenter = volunteerCenter.GetVolunteerCenterByCategoryBL(int.Parse(Session["UserId"].ToString()), selectedCategory);
                }
                else
                {
                    allVolunteerCenter = volunteerCenter.GetVolunteerByPostCodeBL(int.Parse(Session["UserId"].ToString()), ddl_PostCode.SelectedItem.Text);
                }
            }
            
            DataSet dataSet = new DataSet();
            allVolunteerCenter.Item1.Fill(dataSet);

            rp_Volunteer.DataSource = dataSet;
            rp_Volunteer.DataBind();

            rp_Markers.DataSource = allVolunteerCenter.Item2;
            rp_Markers.DataBind();
        }

        protected void rp_Volunteer_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (rp_Volunteer.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    Label lblFooter = (Label)e.Item.FindControl("lbl_Message");
                    lblFooter.Visible = true;
                }
            }
        }
        
        protected void btn_Rate_Click(object sender, EventArgs e)
        {
            Button rate = (sender as Button);
            int rating = int.Parse(Session["Rating"].ToString());
            int volunteerId = int.Parse(rate.CommandArgument);
            int userId = int.Parse(Session["UserId"].ToString());

            RatingLogic ratingLogic = new RatingLogic();

            if (ratingLogic.DoesRatingExistBL(userId, volunteerId))
                ratingLogic.UpdateRatingBL(userId, volunteerId, rating);
            else
                ratingLogic.AddRatingBL(userId, volunteerId, rating);

            getAllVolunteerCenters();
        }

        protected void rt_Org_Click(object sender, RatingEventArgs e)
        {
            Session["Rating"] = Convert.ToInt16(e.Value);
        }
    }
}