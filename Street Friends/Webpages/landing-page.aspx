﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="landing-page.aspx.cs" Inherits="Street_Friends.Webpages.landing_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to Street Friends</title>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    <link href="../Assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../Assets/css/material-kit.css" rel="stylesheet" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script> 
</head>
<body class="landing-page">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="landing-page.aspx">Street Friends</a>
            </div>

            <div class="collapse navbar-collapse" id="navigation-example">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="landing-page.aspx">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Understanding Homeless
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="homeless-list.aspx">Homeless Stories</a></li>
                            <li><a href="stats.aspx">Homeless Facts</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Helping Homeless
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="volunteer-list.aspx">Find Volunteering Opportunities</a></li>
                            <li><a href="support-service.aspx">Support Service for Homeless</a></li>
                            <li><a href="employ-service.aspx">Employment Service for Homeless</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="experience.aspx">Sharing Volunteering Experience</a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><asp:Label ID="lb_Name" runat="server" Text="User"></asp:Label>
    	                        <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><asp:HyperLink ID="hl_One" runat="server"></asp:HyperLink></li>
                            <li><asp:HyperLink ID="hl_Two" runat="server"></asp:HyperLink></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="wrapper">
        <div class="header" style="background-image: url('https://www.sott.net/image/s2/48595/full/helping_hand.jpg');">
            <div class="container">
                <div class="col-md-7">
                    <h4 style="color:#434343"><span style="font-size:xx-large">StreetFriends</span> is your one-stop shop for volunteering for the homeless. It <b>enables</b> volunteers to <b>assist</b> homeless people <b>better</b> understanding their issues and providing them with information about the nearest <b><i>volunteering centres</i></b> and <b><i>support services</i></b> such as health, accommodation, employment and food.</h4>
                    <h4 style="color:#434343"><b>The Benefits of Volunteering</b></h4>
                    <h4 style="color:#434343">
                        1. Gain <b>knowledge</b> about disadvantaged people. <br />
                        2. Boost your own <b>job</b> and <b>career</b> prospects. <br />
                        3. Enjoy a sense of <b>achievement</b> and <b>fulfilment</b> by helping homeless people. <br />
                        4. Develop <b>personally</b> and boost <b>self-esteem.</b> <br />
                        5. Enjoy better <b>physical</b> and <b>mental health.</b> <br />
                        6. Connect and better <b>understand</b> others.
                    </h4>
                    <a class="btn btn-default btn-sm" href="stats.aspx">Reasons for Homelessness</a>
                </div>
            </div>
        </div>

        <div id="mainPage" class="main main-raised">
            <div class="text-center">
                <br />
                <h4><a href="#mainPage" style="color:#434343">Click for More Info</a></h4>
            </div>
            <div class="container">
                <div class="text-center section-landing">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-7">
                                <canvas id="myChart"></canvas>
                            </div>
                            <div class="col-md-5 pull-right" style="text-align:left">
                                <h2><strong>Did You Know</strong></h2>
                                <h2 style="color:#999999"><strong>84%</strong></h2>
                                <h4 style="color:#999999">of the Homeless people suffer from domestic violence, relationship, accommodation issues and financial difficulties.</h4>
                                <h6 class="pull-right"><a href="stats.aspx" style="text-decoration:underline; color:#999999;"> + Learn More</a> &nbsp;&nbsp;&nbsp;&nbsp; </h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-success">
                                <i class="material-icons">verified_user</i>
                            </div>
                            <h4 class="info-title">Understanding Homeless People</h4>
                            <p>See the Facts and Stories of Homeless People to understand the real situation of Homeless People</p>
                            <a class="btn btn-default btn-sm" href="homeless-list.aspx">Homeless Stories</a>
                            <a class="btn btn-default btn-sm" href="stats.aspx">Homeless Facts</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-warning">
                                <i class="material-icons">accessibility</i>
                            </div>
                            <h4 class="info-title">Helping Homeless People</h4>
                            <p>Find information and seek Volunteering Oppoptunities to support Homeless People</p>
                            <a class="btn btn-default btn-sm" href="volunteer-list.aspx">Find Volunteering Opportunities</a>
                            <a class="btn btn-default btn-sm" href="support-service.aspx">Supporting Services for Homeless</a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-danger">
                                <i class="material-icons">forum</i>
                            </div>
                            <h4 class="info-title">Sharing Volunteering Experience</h4>
                            <p>Share the Volunteering Experience to engage other Civilians become potential Volunteer</p>
                            <a class="btn btn-default btn-sm" href="experience.aspx">Sharing Experience</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="team.aspx">Team Inspire</a>
                        </li>
                        <li>
                            <a href="feedback.aspx">Feedback</a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; 2017 - Made by Team Inspire
                </div>
            </div>
        </footer>
    </div>
</body>

<!--   Core JS Files   -->
<script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
<script src="../Assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../Assets/js/material.min.js"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="../Assets/js/material-kit.js" type="text/javascript"></script>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');

    var stateBasedData = {
        labels: ["Australian Capital Territory", "New South Wales", "Queensland", "South Australia", "Tasmania", "Victoria", "Western Australia"],
        datasets: [{
            data: [50.0, 40.8, 45.8, 37.5, 31.9, 42.6, 42.8],
            backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(95, 292, 192, 0.2)', 'rgba(93, 136, 205, 0.2)', 'rgba(23, 112, 80, 0.2)', 'rgba(113, 26, 225, 0.2)'],
            borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 159, 64, 1)', 'rgba(95, 292, 192, 1)', 'rgba(93, 136, 205, 1)', 'rgba(23, 112, 80, 1)', 'rgba(113, 26, 225, 1)'],
            borderWidth: 1
        }]
    };

    var stateBasedOptions = {
        scales: { yAxes: [{ ticks: { beginAtZero: true }}]},
        legend: { display: false },
        title: { display: true, text: 'State Based Infographics - Rate per 10,000 population' },
        onClick: function (e) {

        }
    }
       
    var victoriaData = {
        labels: ["Relationship Issue", "Accomodation Issue", "Financial Difficulties", "Health", "Other"],
        datasets: [{
            data: [33, 29, 22, 3, 14],
            backgroundColor: ['rgba(255, 99, 132, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(255, 159, 64, 0.2)', 'rgba(95, 292, 192, 0.2)', 'rgba(153, 106, 255, 0.2)'],
            borderColor: ['rgba(255,99,132,1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 159, 64, 1)', 'rgba(95, 292, 192, 1)', 'rgba(153, 106, 255, 1)'],
            borderWidth: 1
        }]
    };

    var otherOptions = {
        scales: { yAxes: [{ ticks: { beginAtZero: true } }] },
        legend: { display: false },
        title: { display: true, text: 'Reasons for Homelessness - Data from Homelessness Australia' }
    }

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: victoriaData,
        options: otherOptions
    });
</script>

</html>
