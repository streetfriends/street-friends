﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="employ-service-detail.aspx.cs" Inherits="Street_Friends.Webpages.employ_service_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Employment Service Detail</title>
    <link href="../Assets/css/googlemap.css" rel="stylesheet" />
    
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function initMap() {
            var uluru = { lat: <%=latitude%>, lng: <%=longitude%>};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: uluru
            });

            var contentString = "<h4><strong><%=name%></strong></h4><h5><strong>Address:</strong> <%=address%></h5>"

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });

            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyQtlz9311EcdiMUeQWNl5Tk5RhkDrg5M&callback=initMap" type="text/javascript"></script>   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="profile">
                        <div class="name">
                            <h3 class="title">
                                <asp:Label ID="lb_Name" runat="server"></asp:Label>
                                <div class="pull-right">
                                    <asp:Button ID="btn_Back" runat="server" Text="Go Back" CssClass="btn btn-info btn-round" OnClick="btn_Back_Click" />
                                </div>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2">Address:</label>
                        <div class="col-md-5">
                            <asp:Label ID="lb_Address" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Phone Number:</label>
                        <div class="col-md-5">
                            <asp:Label ID="lb_Phone" runat="server" Text="Not Available"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">URL:</label>
                        <div class="col-md-5">
                            <asp:HyperLink ID="hl_URL" runat="server" Target="_blank" Text="Not Available"></asp:HyperLink>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2">Email:</label>
                        <div class="col-md-5">
                            <asp:HyperLink ID="hl_Email" runat="server" Text="Not Available"></asp:HyperLink>
                        </div>
                    </div>
                    <div id="map"></div>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
