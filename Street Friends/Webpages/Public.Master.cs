﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Street_Friends.Webpages
{
    public partial class Public : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (int.Parse(Session["UserId"].ToString()) == 0 && Session["FullName"] == null)
            {
                hl_One.Text = "Login";
                hl_One.NavigateUrl = "login.aspx";

                hl_Two.Text = "Register";
                hl_Two.NavigateUrl = "register.aspx";
            }
            else
            {
                lb_Name.Text = Session["FullName"].ToString();
                hl_One.Text = "Profile";
                hl_One.NavigateUrl = "profile.aspx";

                hl_Two.Text = "Logout";
                hl_Two.NavigateUrl = "logout.aspx";
            }
        }
    }
}