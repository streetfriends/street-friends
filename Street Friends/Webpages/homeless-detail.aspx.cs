﻿using System;
using System.Net.Http;
using System.Web;
using Model;
using BusinessLogic;

namespace Street_Friends.Webpages
{
    public partial class homeless_detail : System.Web.UI.Page
    {
        public Homeless homeless;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            int homelessID = int.Parse(Request.QueryString["userID"].ToString());
            HomelessLogic homelessLogic = new HomelessLogic();

            homeless = new Homeless();
            homeless = homelessLogic.GetHomelessByIDBL(homelessID);
            homeless.analyzeKey = "https://www.videoindexer.ai/embed/insights/"+homeless.analyzeKey+"/?widgets=keywords,sentiments,transcript";
        }

        protected void btn_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("homeless-list.aspx");
        }
    }
}