﻿using System;

namespace Street_Friends.Webpages
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UserId"] = 0;
            Session["FullName"] = null;

            Response.Redirect("login.aspx");
        }
    }
}