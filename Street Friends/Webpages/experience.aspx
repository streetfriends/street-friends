﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="experience.aspx.cs" Inherits="Street_Friends.Webpages.experience" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Share your Experience</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                        <% if (isAlreadyFavorite) { %>
                                <br />
                                <div class="alert alert-info">
                                    <div class="container-fluid">
	                                    <div class="alert-icon">
		                                    <i class="material-icons">info_outline</i>
	                                    </div>
	                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                    </button>
	                                    <b>Message:</b> This <strong>experience</strong> has <strong>already been</strong> added as a <strong>Favorite</strong>.
                                    </div>
                                </div>
                        <% }
                           else
                           {
                               if (isFavorite) { %>
                                    <br />
                                    <div class="alert alert-success">
                                        <div class="container-fluid">
	                                        <div class="alert-icon">
		                                        <i class="material-icons">check</i>
	                                        </div>
	                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                        </button>
	                                        <b>Message:</b> You have <strong>sucessfully</strong> added a <strong>Favorite Experience</strong>.
                                        </div>
                                    </div>
                        <%      }
                            }
                        %>
                        <% if (isLogin == 0){ %>
                                <br />
                                <div class="alert alert-info">
                                    <div class="container-fluid">
	                                    <div class="alert-icon">
		                                    <i class="material-icons">info_outline</i>
	                                    </div>
	                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                    </button>
	                                    <b>Message:</b> You need to Login to share Experience. <a href="login.aspx"><strong>Click Here</strong></a> to login.
                                    </div>
                                </div>
                        <%  }
                            else
                            {
                                if (isPosted == 1) { %>
                                    <br />
                                    <div class="alert alert-info">
                                        <div class="container-fluid">
	                                        <div class="alert-icon">
		                                        <i class="material-icons">info</i>
	                                        </div>
	                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                        </button>
                                            <b>Message:</b> Your <strong>Experience</strong> has been Added and will be reviewed by a <strong>Moderator</strong> before made <strong>Public</strong>.
                                        </div>
                                    </div>
                            <%
                                    }
                                    else if (isPosted == -1)
                                    {
                            %>
                                    <br />
                                    <div class="alert alert-danger">
                                        <div class="container-fluid">
	                                        <div class="alert-icon">
	                                            <i class="material-icons">error_outline</i>
	                                        </div>
	                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                        <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                        </button>
                                            <b>Sorry:</b> Your Experience cannot be shared because you are not complying with the terms of use by using Inappropiate Language 
                                        </div>
                                    </div>
                            <%
                                    }
                                }
                            %>
                            <div class="name">
                                <h3 class="title">Share your Experience</h3>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <% if (isLogin == 1) { %>
                            <asp:RequiredFieldValidator ID="rf_Experience" runat="server" ErrorMessage="Enter Your Experience" ControlToValidate="tb_experience" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <div class="form-group label-floating">
                                <label class="control-label">Write your Experience Here ...</label>
                                <asp:Textbox ID="tb_experience" runat="server" TextMode="MultiLine" CssClass="form-control" rows="5"></asp:Textbox>
                            </div>
                            <asp:RegularExpressionValidator ID="rev_uploadimage" runat="server" ErrorMessage="Only Images are Allowed" Display="Dynamic" ControlToValidate="fu_Image" ForeColor="Red" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"></asp:RegularExpressionValidator>
                            <asp:FileUpload ID="fu_Image" AllowMultiple="false" runat="server" />
                            <br />
                            <asp:Button ID="btn_post" CssClass="btn btn-success" runat="server" Text="Share" OnClick="btn_post_Click"/>
                    
                            <br /><br />
                        <% } %>
                        <div class="col-md-8">
                            <asp:Repeater ID="rp_Experience" runat="server">
                                <ItemTemplate>
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <strong>Name:</strong> 
                                                <%# DataBinder.Eval(Container.DataItem, "user_fullname") %> 
                                                <div class="pull-right">
                                                    <strong>Date:</strong> 
                                                    <%# DataBinder.Eval(Container.DataItem, "date") %> <%# DataBinder.Eval(Container.DataItem, "time") %> 
                                                </div> 
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <%# DataBinder.Eval(Container.DataItem, "description") %>
                                            <br /><br />
                                            <img src='../Assets/experience/<%# DataBinder.Eval(Container.DataItem, "experience_id") %>.jpg'/>
                                            <br />
                                            <% if (isLogin == 1) { %>
                                                <b><asp:Label ID="lbl_Message" runat="server" Text="Liked the Experience? Share it on your Profile by pressing" Visible='<%# DataBinder.Eval(Container.DataItem, "isFavorite").ToString() == "0" ? true : false %>'></asp:Label></b>
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:LinkButton ID="btn_favorite" OnClick="btn_favorite_Click" runat="server" CssClass="btn btn-primary btn-round" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "experience_id") %>' CausesValidation="false" Text='<%# DataBinder.Eval(Container.DataItem, "isFavorite").ToString() == "0" ? "<i class=material-icons>favorite_border</i> Like" : " <i class=material-icons>favorite</i> Liked" %>'></asp:LinkButton>
                                            <% } %>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="col-md-4">
                            <a class="twitter-timeline" data-tweet-limit="5" href="https://twitter.com/HomelessAU">Tweets by HomelessAU</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</asp:Content>
