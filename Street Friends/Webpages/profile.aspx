﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="Street_Friends.Webpages.profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Your Profile</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                            <div class="name">
                                <h3 class="title">Profile - <asp:Label runat="server" ID="lb_Name"></asp:Label>  </h3>
                            </div>
                        </div>
                    </div>
                    <% if (isDeleted) { %>
                        <div class="alert alert-success">
                            <div class="container-fluid">
	                            <div class="alert-icon">
		                            <i class="material-icons">check</i>
	                            </div>
	                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                            </button>
                                <b>Message:</b> Your <strong>Experience</strong> has been <strong>Deleted Sucessfully</strong>.
                            </div>
                        </div>
                    <% } %>
                    <% if (!isPasswordSame) { %>
                        <div class="alert alert-danger">
                            <div class="container-fluid">
	                            <div class="alert-icon">
		                            <i class="material-icons">error_outline</i>
	                            </div>
	                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                            <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                            </button>
                                <b>Message:</b> Sorry. Your <strong> Current Password </strong> does not match the Password you <strong> just entered.</strong>
                            </div>
                        </div>
                    <%  }
                        else
                        {
                            if (isPasswordUpdated)
                            { 
                    %>
                                <div class="alert alert-success">
                                    <div class="container-fluid">
	                                    <div class="alert-icon">
		                                    <i class="material-icons">check</i>
	                                    </div>
	                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
	                                    </button>
                                        <b>Message:</b> Your <strong>Password</strong> has been <strong>changed Sucessfully</strong>.
                                    </div>
                                </div>
                    <%      
                            }
                        } 
                    %>
                    <div class="row">
                        <div class="card card-nav-tabs">
                            <div class="header header-info" style="height:75px">
                                <div class="nav-tabs-navigation">
									<div class="nav-tabs-wrapper">
										<ul class="nav nav-tabs" data-tabs="tabs">
											<li class="active">
												<a href="#profile" data-toggle="tab" aria-expanded="true">
													<i class="material-icons">face</i>
													Profile
												<div class="ripple-container"></div></a>
											</li>
                                            <li>
												<a href="#yourexperience" data-toggle="tab">
													<i class="material-icons">chat</i>
													Your Experience
												<div class="ripple-container"></div></a>
											</li>
                                            <li>
												<a href="#favorite" data-toggle="tab">
													<i class="material-icons">favorite</i>
													Favorite Experiences
												<div class="ripple-container"></div></a>
											</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
								<div class="tab-content text-center">
									<div class="tab-pane active" id="profile">
										<div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-2"><strong>Email</strong></label>
                                                <div class="col-sm-2">
                                                    <asp:Label runat="server" ID="lb_Email"></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2"><strong>Update Password</strong></label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox runat="server" ID="tb_OldPassword" CssClass="form-control" TextMode="Password" placeholder="Enter your Old Password"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:RequiredFieldValidator ID="rf_OldPassword" runat="server" ErrorMessage="Enter Old Password" ControlToValidate="tb_OldPassword" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox runat="server" ID="tb_NewPassword" CssClass="form-control" TextMode="Password" placeholder="Enter your New Password"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:RequiredFieldValidator ID="rf_NewPassword" runat="server" ErrorMessage="Enter New Password" ControlToValidate="tb_NewPassword" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox runat="server" ID="tb_Confirm" CssClass="form-control" TextMode="Password" placeholder="Confirm your New Password"></asp:TextBox>
                                                </div>
                                                <div class="col-sm-2">
                                                    <asp:RequiredFieldValidator ID="rf_Confirm" runat="server" ErrorMessage="Re-Enter your Password" ControlToValidate="tb_NewPassword" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="cv_Match" ControlToValidate="tb_Confirm" ControlToCompare="tb_NewPassword" runat="server" ErrorMessage="Passwords do not match" Display="Dynamic" ForeColor="Red" Operator="Equal" Type="String"></asp:CompareValidator>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2"></label>
                                                <div class="col-sm-1">
                                                    <asp:Button runat="server" ID="btn_Update" CssClass="btn btn-success" Text="Update" OnClick="btn_Update_Click" />
                                                </div>
                                            </div>
                                        </div>
									</div>
                                    <div class="tab-pane" id="yourexperience">
										<asp:Repeater ID="rp_Experience" runat="server" OnItemDataBound="rp_Experience_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title" style="text-align:left"> 
                                                            <strong>Name:</strong> <%# DataBinder.Eval(Container.DataItem, "user_fullname") %> 
                                                            <div class="pull-right" style="margin-top:-10px">
                                                                <strong>Date:</strong> <%# DataBinder.Eval(Container.DataItem, "date") %> <%# DataBinder.Eval(Container.DataItem, "time") %> &nbsp;&nbsp;&nbsp;
                                                                <asp:Button ID="btn_delete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClick="btn_delete_Click" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "experience_id") %>' CausesValidation="false" />
                                                            </div>
                                                        </h3>
                                                    </div>
                                                    <div class="panel-body" style="text-align:left">
                                                        <%# DataBinder.Eval(Container.DataItem, "description") %>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_Message" Text="Sorry. You have not shared any Experiences yet." runat="server" Visible="false"></asp:Label>
                                            </FooterTemplate>
                                        </asp:Repeater>
									</div>
                                    <div class="tab-pane" id="favorite">
										<asp:Repeater ID="rp_Favorite" runat="server" OnItemDataBound="rp_Favorite_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="panel panel-warning">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title" style="text-align:left"> 
                                                            <strong>Name:</strong> <%# DataBinder.Eval(Container.DataItem, "user_fullname") %> 
                                                            <div class="pull-right">
                                                                <strong>Date:</strong> <%# DataBinder.Eval(Container.DataItem, "date") %> <%# DataBinder.Eval(Container.DataItem, "time") %>
                                                            </div>
                                                        </h3>
                                                    </div>
                                                    <div class="panel-body" style="text-align:left">
                                                        <%# DataBinder.Eval(Container.DataItem, "description") %>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lbl_Message" Text="Sorry. You don't have any Favorite Experiences." runat="server" Visible="false"></asp:Label>
                                            </FooterTemplate>
                                        </asp:Repeater>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
   </div>
</asp:Content>
