﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="team.aspx.cs" Inherits="Street_Friends.Webpages.team" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Team Inspire</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="container">
                <div class="section text-center">
                    <h2 class="title">Here is our team</h2>
                    <div class="team">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="team-player">
                                    <img src="../Assets/img/abdullah.png" alt="Thumbnail Image" class="img-raised img-circle" />
                                    <h4 class="title">Abdullah Shabbir
                                <br />
                                        <small class="text-muted">Developer</small>
                                    </h4>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-twitter"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-instagram"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-facebook-square"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="team-player">
                                    <img src="../Assets/img/reem.png" alt="Thumbnail Image" class="img-raised img-circle" />
                                    <h4 class="title">Reem Alssadi<br />
                                        <small class="text-muted">Designer & Facilitator</small>
                                    </h4>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-twitter"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-linkedin"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="team-player">
                                    <img src="../Assets/img/wesley.jpg" alt="Thumbnail Image" class="img-raised img-circle" />
                                    <h4 class="title">Wuyu Li<br />
                                        <small class="text-muted">Business Analyst</small>
                                    </h4>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-google-plus"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-youtube-play"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="team-player">
                                    <img src="../Assets/img/carlos.jpg" alt="Thumbnail Image" class="img-raised img-circle" />
                                    <h4 class="title">Carlos Orjuela<br />
                                        <small class="text-muted">IT Support & Cloud Management</small>
                                    </h4>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-google-plus"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-youtube-play"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-twitter"></i></a>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="team-player">
                                    <img src="../Assets/img/feng.jpg" alt="Thumbnail Image" class="img-raised img-circle" />
                                    <h4 class="title">Weicong Feng<br />
                                        <small class="text-muted">Tester & UI/UX</small>
                                    </h4>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-google-plus"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon"><i class="fa fa-youtube-play"></i></a>
                                    <a href="#pablo" class="btn btn-simple btn-just-icon btn-default"><i class="fa fa-twitter"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
