﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="employ-service.aspx.cs" Inherits="Street_Friends.Webpages.employ_service" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Employment Service</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                            <div class="name">
                                <h3 class="title">Employment Service for Homeless</h3>
                            </div>
                            <div>
                                <h5>Put a roof over the Homeless people by helping them find Employment service in Victoria</h5>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <asp:Repeater ID="rp_Employment" runat="server">
                            <ItemTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <%# DataBinder.Eval(Container.DataItem, "employment_name") %>
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-md-9">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-2"><strong>Description:</strong></label>
                                                    <div class="col-md-10">
                                                        <%# DataBinder.Eval(Container.DataItem, "employment_desc") %> 
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2"><strong>Address:</strong></label>
                                                    <div class="col-md-10">
                                                        <%# DataBinder.Eval(Container.DataItem, "employment_address") %> 
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2"><strong>Phone:</strong></label>
                                                    <div class="col-md-4">
                                                        <%# DataBinder.Eval(Container.DataItem, "employment_phone") %>
                                                    </div>
                                                    <label class="col-md-2"><strong>Email:</strong></label>
                                                    <div class="col-md-4">
                                                        <a href='mailTo:<%# DataBinder.Eval(Container.DataItem, "employment_email") %>'><%# DataBinder.Eval(Container.DataItem, "employment_email") %></a>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2"><strong>Website:</strong></label>
                                                    <div class="col-md-10">
                                                        <a href='http://<%# DataBinder.Eval(Container.DataItem, "employment_website") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "employment_website") %></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <a class="twitter-timeline" data-tweet-limit="1" href="https://twitter.com/<%# DataBinder.Eval(Container.DataItem, "employment_twitter") %>">Tweets by <%# DataBinder.Eval(Container.DataItem, "employment_twitter") %></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
