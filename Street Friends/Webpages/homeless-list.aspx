﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="homeless-list.aspx.cs" Inherits="Street_Friends.Webpages.homeless_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="profile">
                        <div class="name">
                            <h3 class="title">Homeless Stories</h3>
                            <p>See the stories of Homeless people to understand the real situation of them</p>
                        </div>
                    </div>
                    <asp:Repeater ID="rp_homeless" runat="server">
                        <ItemTemplate>
                            <table class="team">
                                <tr class="team-player">
                                    <td class="col-md-2"><img src="<%# DataBinder.Eval(Container.DataItem, "homeless_image") %>" alt="Thumbnail Image" class="img-raised img-rounded" /></td>
                                    <td><h4><a href='homeless-detail.aspx?userID=<%# DataBinder.Eval(Container.DataItem, "homeless_id") %>'><strong><%# DataBinder.Eval(Container.DataItem, "homeless_name") %></strong></a></h4> <%# DataBinder.Eval(Container.DataItem, "homeless_description") %> <br /><br /> <a href='homeless-detail.aspx?userID=<%# DataBinder.Eval(Container.DataItem, "homeless_id") %>'>Click to Watch Video</a> </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
