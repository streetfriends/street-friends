﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="homeless-detail.aspx.cs" Inherits="Street_Friends.Webpages.homeless_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Homeless Details</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="profile">
                        <div class="name">
                            <h3 class="title">
                                Story of <%=homeless.homelessName%>
                                <div class="pull-right">
                                    <asp:Button ID="btn_Back" runat="server" Text="Go Back" CssClass="btn btn-info btn-round" OnClick="btn_Back_Click" />
                                </div>
                            </h3>
                            
                        </div>
                    </div>
                    
                    
                    <br />
                    <p><strong>Keywords</strong> are used to describe the key topics discussed in the video by the person.
                    <strong>Speech Sentiments</strong> shows both the <span style="color:green"><strong>Positive</strong></span>, <span style="color:red"><strong>Negative</strong></span> and <span style="color:gray"><strong>Neutral</strong></span> parts of the Speech and helps us understand which parts of the speech were good or bad situations.</p>
                </div>             
            </div>
            <div class="row">
                <div class="form-inline">
                    <div class="col-sm-7" style="margin-left:10px;">
                        <iframe height="500" src="<%=homeless.youtubeURL%>" frameborder="0" allowfullscreen style="width:112%"></iframe>
                    </div>
                    <div class="col-sm-4" style="margin-left:5em;">
                        <iframe height="500" src="<%=homeless.analyzeKey%>" frameborder="0" style="width:100%"></iframe>
                    </div>
                </div>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
