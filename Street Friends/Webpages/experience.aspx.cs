﻿using System;
using Model;
using BusinessLogic;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;

namespace Street_Friends.Webpages
{
    public partial class experience : System.Web.UI.Page
    {
        CensorLogic censor;
        public int isPosted = 0;
        public int isLogin = 0;
        public bool isAlreadyFavorite = false;
        public bool isFavorite = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                censor = new CensorLogic();
            }

            showExperience();

            if (int.Parse(Session["UserId"].ToString()) == 0 && Session["FullName"] == null)
                isLogin = 0;
            else
                isLogin = 1;
        }

        protected void btn_post_Click(object sender, EventArgs e)
        {
            isPosted = censor.CensorText(tb_experience.Text);

            if (isPosted == 1)
            {
                Experience myExperience = new Experience();
                ExperienceLogic experience = new ExperienceLogic();

                myExperience.postDescription = tb_experience.Text.Replace(Environment.NewLine, "<br />");
                myExperience.postDate = DateTime.Now.ToShortDateString();
                myExperience.postTime = DateTime.Now.ToShortTimeString();
                myExperience.userId = int.Parse(Session["UserId"].ToString());

                experience.AddExperienceBL(myExperience);

                tb_experience.Text = string.Empty;

                showExperience();
            }
        }

        public void showExperience()
        {
            ExperienceLogic experience = new ExperienceLogic();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            adapter = experience.ShowExperienceBL(int.Parse(Session["UserId"].ToString()));
            adapter.Fill(dataSet, "Experience");
            rp_Experience.DataSource = dataSet.Tables["Experience"];
            rp_Experience.DataBind();
        }

        protected void btn_favorite_Click(object sender, EventArgs e)
        {
            int result= 0;
            int userId = int.Parse(Session["UserId"].ToString());
            LinkButton hyperLink = (sender as LinkButton);
            int experienceId = int.Parse(hyperLink.CommandArgument);

            ExperienceLogic experienceLogic = new ExperienceLogic();

            if (!experienceLogic.IsAlreadyFavoriteBL(userId, experienceId))
                result = experienceLogic.AddFavoriteExperienceBL(userId, experienceId);
            else
                isAlreadyFavorite = true;

            isFavorite = result == 1 ? true : false;

            showExperience();
        }

    }
}