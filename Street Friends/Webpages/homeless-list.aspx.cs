﻿using System;
using BusinessLogic;
using System.Data;
using System.Data.SqlClient;

namespace Street_Friends.Webpages
{
    public partial class homeless_list : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HomelessLogic homelessLogic = new HomelessLogic();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            
            adapter = homelessLogic.GetAllHomelessBL();
            adapter.Fill(dataSet, "HomelessList");
            rp_homeless.DataSource = dataSet.Tables["HomelessList"];
            rp_homeless.DataBind();
        }
    }
}