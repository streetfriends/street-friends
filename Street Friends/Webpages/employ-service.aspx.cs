﻿using System;
using BusinessLogic;
using System.Data.SqlClient;
using System.Data;

namespace Street_Friends.Webpages
{
    public partial class employ_service : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EmploymentLogic employmentLogic = new EmploymentLogic();
            SqlDataAdapter adapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();

            adapter = employmentLogic.GetAllEmploymentServiceBL();
            adapter.Fill(dataSet, "Employment");
            rp_Employment.DataSource = dataSet.Tables["Employment"];
            rp_Employment.DataBind();
        } 
    }
}