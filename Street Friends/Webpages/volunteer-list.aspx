﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="volunteer-list.aspx.cs" Inherits="Street_Friends.Webpages.volunteer_list" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Volunteering</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css" />
    
    <link href="../Assets/css/rating.css" rel="stylesheet" type="text/css" />
    <link href="../Assets/css/googlemap.css" rel="stylesheet" />

    <script type="text/javascript">
        var markers = [
            <asp:Repeater ID="rp_Markers" runat="server">
                <ItemTemplate>
                    {
                        "title": '<%# DataBinder.Eval(Container.DataItem, "volunteer_name") %>',
                        "lat": '<%# DataBinder.Eval(Container.DataItem, "volunteer_latitude") %>',
                        "lng": '<%# DataBinder.Eval(Container.DataItem, "volunteer_longitude") %>',
                        "description": '<%# DataBinder.Eval(Container.DataItem, "volunteer_description") %>'
                    }
                </ItemTemplate>
                <SeparatorTemplate>,</SeparatorTemplate>
            </asp:Repeater >
        ];
    </script>

    <script type="text/javascript">
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(-37.722668, 144.5995713),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                google.maps.event.trigger(map, "resize");
            });

            var locations = [];
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                var marker = new google.maps.Marker({
                    'position': myLatlng,
                    'map': map,
                    'title': data.title
                });

                locations.push(marker);

                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.title);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }

            var markerCluster = new MarkerClusterer(map, locations, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
        }
    </script>

    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyQtlz9311EcdiMUeQWNl5Tk5RhkDrg5M" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                            <div class="name">
                                <h3 class="title">Find Volunteering Opportunities</h3>
                                <div><h5>Volunteering for Homeless people is very important. Find a volunteering opportunity to make a real difference to their lives</h5></div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#list" aria-controls="list" role="tab" data-toggle="tab">
                                    <i class="material-icons">list</i>
                                    Volunteering Center List
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#myMap" aria-controls="myMap" role="tab" data-toggle="tab">
                                    <i class="material-icons">map</i>
                                    Map
                                </a>
                            </li>
                        </ul>
                        <br />
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="list">
                                <table>
                            <thead>
                                <tr>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                    <td style="width:2%"></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>Categories:</strong></td>
                                    <td><asp:CheckBox runat="server" ID="cb_All" CssClass="checkbox-inline" Text="All" Checked="true"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Accomodation" CssClass="checkbox-inline" Text="Accomodation"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Health" CssClass="checkbox-inline" Text="Health"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Financial" CssClass="checkbox-inline" Text="Financial"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Children" CssClass="checkbox-inline" Text="Children"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Women" CssClass="checkbox-inline" Text="Women"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Legal" CssClass="checkbox-inline" Text="Legal"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Drugs" CssClass="checkbox-inline" Text="Alcohol / Drugs"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Training" CssClass="checkbox-inline" Text="Training"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Support" CssClass="checkbox-inline" Text="Support"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Food" CssClass="checkbox-inline" Text="Food"/></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Cloths" CssClass="checkbox-inline" Text="Cloths"/></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><asp:CheckBox runat="server" ID="cb_Employment" CssClass="checkbox-inline" Text="Employment"/></td>
                                </tr>
                                <tr>
                                    <td><strong>PostCode:</strong></td>
                                    <td colspan="1"> 
                                        <asp:DropDownList data-live-search="true" AppendDataBoundItems="true" CssClass="selectpicker" runat="server" ID="ddl_PostCode" DataTextField="postcode_name" DataValueField="postcode_id">
                                            <asp:ListItem Text="All" Value="0" />
                                        </asp:DropDownList> 
                                    </td>
                                    <td colspan="5">
                                        <asp:Button runat="server" ID="btn_Search" CssClass="btn btn-success" Text="Search" OnClick="btn_Search_Click"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">
                                       <div class="row">
                                            <asp:Repeater ID="rp_Volunteer" runat="server" OnItemDataBound="rp_Volunteer_ItemDataBound">
                                                <ItemTemplate>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title">
                                                                <%# DataBinder.Eval(Container.DataItem, "volunteer_name") %>
                                                            </h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div class="col-md-9">
                                                                <div class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Description:</strong></label>
                                                                        <div class="col-md-10">
                                                                            <%# DataBinder.Eval(Container.DataItem, "volunteer_description") %> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Address:</strong></label>
                                                                        <div class="col-md-10">
                                                                            <%# DataBinder.Eval(Container.DataItem, "volunteer_address") %> 
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Phone:</strong></label>
                                                                        <div class="col-md-4">
                                                                            <%# DataBinder.Eval(Container.DataItem, "volunteer_phone") %>
                                                                        </div>
                                                                        <label class="col-md-2"><strong>Email:</strong></label>
                                                                        <div class="col-md-4">
                                                                            <a href='mailTo:<%# DataBinder.Eval(Container.DataItem, "volunteer_email") %>'><%# DataBinder.Eval(Container.DataItem, "volunteer_email") %></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Website:</strong></label>
                                                                        <div class="col-md-10">
                                                                            <a href='http://<%# DataBinder.Eval(Container.DataItem, "volunteer_website") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "volunteer_website") %></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Categories:</strong></label>
                                                                        <div class="col-md-10">
                                                                            <%# DataBinder.Eval(Container.DataItem, "category_list") %>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-2"><strong>Rate:</strong></label>
                                                                        <%if (isLogin == 0)
                                                                            { %>       
                                                                            <div class="col-md-3">
                                                                                <ajaxToolkit:Rating ID="rt_Org" runat="server" MaxRating="5" CurrentRating='<%# int.Parse(DataBinder.Eval(Container.DataItem, "rating").ToString().Substring(0,1)) %>' StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar" FilledStarCssClass="filledRatingStar" EmptyStarCssClass="emptyRatingStar" OnClick="rt_Org_Click" />
                                                                            </div>
                                                                        <% } else { %>
                                                                            <div class="col-md-3">
                                                                                <ajaxToolkit:Rating ID="Rating1" runat="server" MaxRating="5" CurrentRating='<%# DataBinder.Eval(Container.DataItem, "yourrating") %>' StarCssClass="ratingStar" WaitingStarCssClass="savedRatingStar" FilledStarCssClass="filledRatingStar" EmptyStarCssClass="emptyRatingStar" OnClick="rt_Org_Click" />
                                                                            </div>
                                                                        <% } %>
                                                                        <div class="col-md-5">
                                                                            <strong>Average Rating:</strong> <%# DataBinder.Eval(Container.DataItem, "rating") %> &nbsp;&nbsp;&nbsp;&nbsp; <strong>No. of Votes:</strong> <%# DataBinder.Eval(Container.DataItem, "votes") %>
                                                                        </div>
                                                                    </div>
                                                                    <%if (isLogin == 1) { %>
                                                                        <div class="form-group">
                                                                            <div class="col-md-1"></div>
                                                                            <div class="col-md-5">
                                                                                <asp:Button ID="btn_Rate" runat="server" CssClass="btn btn-info btn-round" OnClick="btn_Rate_Click" Text="Rate" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "volunteer_id") %>' />
                                                                            </div>
                                                                        </div>
                                                                    <% } %>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <a class="twitter-timeline" data-tweet-limit="1" href="https://twitter.com/<%# DataBinder.Eval(Container.DataItem, "volunteer_twitter") %>">Tweets by <%# DataBinder.Eval(Container.DataItem, "volunteer_twitter") %></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    <asp:Label ID="lbl_Message" Text="Sorry. There are no Volunteering Organizations" runat="server" Visible="false"></asp:Label>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                            <div role="tabpanel" class="tab-pane" id="myMap">
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div>
</asp:Content>