﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="Street_Friends.Webpages.error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Sorry, Something went Wrong</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="container">
                <div class="section text-center section-landing">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="../Assets/img/error.png"/>
                        </div>
                        <div class="col-md-7 pull-right">
                            <h2>Something went wrong</h2>
                            <br />
                            <h3>Try that again, and if it still doesn't work, let us know.</h3>
                            <a href="feedback.aspx" class="btn btn-info btn-round">
                                <i class="material-icons">feedback</i> &nbsp;LET US KNOW
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
