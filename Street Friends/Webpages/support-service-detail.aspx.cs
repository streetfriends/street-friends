﻿using System;
using Model;
using BusinessLogic;

namespace Street_Friends.Webpages
{
    public partial class support_service_detail : System.Web.UI.Page
    {
        public string name;
        public string address;
        public string twitter;
        public float latitude;
        public float longitude;

        public string dayName;
        public bool isTwitterAvaiable = false;
        public bool isLocationAvailable = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            dayName = DateTime.Now.ToString("dddd");

            if (!IsPostBack)
            {
                int supportServiceId = int.Parse(Request.QueryString["supportServiceId"].ToString());
                SupportService supportService = new SupportService();

                SupportServiceLogic supportServiceLogic = new SupportServiceLogic();
                supportService = supportServiceLogic.GetSupportServiceByIDBL(supportServiceId);

                lb_Name.Text = supportService.name;
                lb_Address.Text = supportService.address;
                lb_Phone.Text = supportService.phone;
            
                hl_Email.Text = supportService.email;
                hl_Email.NavigateUrl = "mailto:" + supportService.email;
                
                hl_URL.Text = supportService.url;

                if (!supportService.url.StartsWith("http://"))
                    hl_URL.NavigateUrl = "http://" + supportService.url;
            
                if (supportService.tramRoute != "")
                {
                    lb_Tram.Text = supportService.tramRoute;
                }

                if (supportService.busRoute != "")
                {
                    lb_Bus.Text = supportService.busRoute;
                }
                else
                {
                    lb_BusText.Visible = false;
                    lb_Bus.Visible = false;
                }

                if (supportService.trainStation != "")
                {
                    lb_Station.Text = supportService.trainStation;
                }
                else
                {
                    lb_StationText.Visible = false;
                    lb_Station.Visible = false;
                }

                if (supportService.cost != "")
                {
                    lb_Cost.Text = supportService.cost;
                }
                else
                {
                    lb_CostText.Visible = false;
                    lb_Cost.Visible = false;
                }

                if (supportService.category_1 != "N/A")
                {
                    lb_Categories.Text = supportService.category_1;

                    if (supportService.category_2 != "N/A")
                    {
                        lb_Categories.Text += ", " + supportService.category_2;

                        if (supportService.category_3 != "N/A")
                        {
                            lb_Categories.Text += ", " + supportService.category_3;

                            if (supportService.category_4 != "N/A")
                            {
                                lb_Categories.Text += ", " + supportService.category_4;

                                if (supportService.category_5 != "N/A")
                                {
                                    lb_Categories.Text += ", " + supportService.category_5;
                                }
                            }
                        }
                    }
                }

                if (supportService.monday != "")
                {
                    lb_Monday.Text = supportService.monday;
                    lb_Tuesday.Text = supportService.tuesday;
                    lb_Wednesday.Text = supportService.wednesday;
                    lb_Thursday.Text = supportService.thursday;
                    lb_Friday.Text = supportService.friday;
                }

                if (supportService.saturday != "")
                {
                    lb_Saturday.Text = supportService.saturday;
                }

                if (supportService.sunday != "")
                {
                    lb_Sunday.Text = supportService.sunday;
                }

                if (supportService.publicHoliday != "")
                {
                    lb_PublicHoliday.Text = supportService.publicHoliday;
                }

                name = supportService.name;
                address = supportService.address;

                if (supportService.twitter != "")
                {
                    twitter = supportService.twitter.Replace("@","");
                    isTwitterAvaiable = true;
                }

                if (supportService.latitude != "" && supportService.longitude != "")
                {
                    latitude = float.Parse(supportService.latitude);
                    longitude = float.Parse(supportService.longitude);
                    isLocationAvailable = true;
                }
            }
        }

        protected void btn_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("support-service.aspx");
        }
    }
}