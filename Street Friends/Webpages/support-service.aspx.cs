﻿using System;
using BusinessLogic;
using System.Data.SqlClient;
using System.Data;

namespace Street_Friends.Webpages
{
    public partial class support_service : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SupportServiceLogic supportService = new SupportServiceLogic();
                DataSet dataSet = new DataSet();

                Tuple <SqlDataAdapter, DataTable> allSupportService = supportService.GetAllSupportServiceBL();

                allSupportService.Item1.Fill(dataSet, "SupportService");
                rp_Service.DataSource = dataSet.Tables["SupportService"];
                rp_Service.DataBind();

                rp_Markers.DataSource = allSupportService.Item2;
                rp_Markers.DataBind();
            }
        }
    }
}