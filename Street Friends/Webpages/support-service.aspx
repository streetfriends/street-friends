﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Webpages/Public.Master" AutoEventWireup="true" CodeBehind="support-service.aspx.cs" Inherits="Street_Friends.Webpages.support_service" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Support Service</title>
    <script src="../Assets/js/jquery.min.js" type="text/javascript"></script>

    <link href="../Assets/css/googlemap.css" rel="stylesheet" />

    <script type="text/javascript">
        var markers = [
            <asp:Repeater ID="rp_Markers" runat="server">
                <ItemTemplate>
                    {
                        "title": '<%# DataBinder.Eval(Container.DataItem, "name") %>',
                        "lat": '<%# DataBinder.Eval(Container.DataItem, "latitude") %>',
                        "lng": '<%# DataBinder.Eval(Container.DataItem, "longitude") %>'
                    }
                </ItemTemplate>
                <SeparatorTemplate>,</SeparatorTemplate>
            </asp:Repeater>
        ];
    </script>

    <script type="text/javascript">
        window.onload = function () {
            var mapOptions = {
                center: new google.maps.LatLng(-37.722668, 144.46823312),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                google.maps.event.trigger(map, "resize");
            });

            var locations = [];
            for (i = 0; i < markers.length; i++) {
                var data = markers[i]
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                var marker = new google.maps.Marker({
                    'position': myLatlng,
                    'map': map,
                    'title': data.title
                });

                locations.push(marker);
                
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent(data.title);
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }

            var markerCluster = new MarkerClusterer(map, locations, { imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
        }
    </script>

    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyQtlz9311EcdiMUeQWNl5Tk5RhkDrg5M" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="wrapper">
        <div class="main main-raised">
            <div class="profile-content">
                <div class="container">
                    <div class="row">
                        <div class="profile">
                            <div class="name">
                                <h3 class="title">Support Service for Homeless</h3>
                            </div>
                            <div>
                                <h5>Not all Homeless people may know about the supporitng services available in Victoria. Help them find out these Service</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#list" aria-controls="list" role="tab" data-toggle="tab">
                                    <i class="material-icons">list</i>
                                    Support Service List
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#myMap" aria-controls="myMap" role="tab" data-toggle="tab">
                                    <i class="material-icons">map</i>
                                    Map
                                </a>
                            </li>
                        </ul>
                        <br />
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="list">
                            <div class="col-md-9">
                            <asp:Repeater ID="rp_Service" runat="server">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <%# DataBinder.Eval(Container.DataItem, "name") %> 
                                                <div class="pull-right" style="margin-top:-10px">
                                                    <a class="btn btn-info btn-xs" href="support-service-detail.aspx?supportServiceID=<%# DataBinder.Eval(Container.DataItem, "supportservice_id") %>">
	                                                    <i class="material-icons">list</i> &nbsp; View Detail
                                                    </a>
                                                </div>
                                            </h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-md-2"><strong>Address:</strong></label>
                                                <div class="col-md-10">
                                                    <%# DataBinder.Eval(Container.DataItem, "address") %>, <%# DataBinder.Eval(Container.DataItem, "suburb") %> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2"><strong>Website:</strong></label>
                                                <div class="col-md-4">
                                                    <a href='http://<%# DataBinder.Eval(Container.DataItem, "website") %>' target="_blank"><%# DataBinder.Eval(Container.DataItem, "website") %></a>
                                                </div>
                                                <label class="col-md-2"><strong>Phone:</strong></label>
                                                <div class="col-md-4">
                                                    <%# DataBinder.Eval(Container.DataItem, "phone") %> 
                                                </div>
                                        
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2"><strong>Email:</strong></label>
                                                <div class="col-md-4">
                                                    <a href='mailTo:<%# DataBinder.Eval(Container.DataItem, "email") %>'><%# DataBinder.Eval(Container.DataItem, "email") %></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        </div>
                            <div role="tabpanel" class="tab-pane col-md-9" id="myMap">
                                <div id="map"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a class="twitter-timeline" data-tweet-limit="5" href="https://twitter.com/YOTSAustralia">Tweets by YouthOffTheStreets</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        </div>
    </div> 
</asp:Content>
