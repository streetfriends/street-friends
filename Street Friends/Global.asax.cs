﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Street_Friends
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session["UserId"] = 0;
            Session["FullName"] = null;
            Session["Rating"] = null;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            //Response.Redirect("../Webpages/error.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Session["UserId"] = 0;
            Session["FullName"] = null;
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Session["UserId"] = 0;
            Session["FullName"] = null;
        }
    }
}