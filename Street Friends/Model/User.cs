﻿namespace Model
{
    public class User
    {
        public int userId { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}