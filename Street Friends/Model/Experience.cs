﻿
namespace Model
{
    public class Experience
    {
        public string postId { get; set; }
        public string postDescription { get; set; }
        public string postDate { get; set; }
        public string postTime { get; set; }
        public int userId { get; set; }
    }
}