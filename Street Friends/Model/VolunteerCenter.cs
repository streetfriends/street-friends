﻿namespace Model
{
    public class VolunteerCenter
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string twitter { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public string category { get; set; }
    }
}