﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model
{
    public class Homeless
    {
        public int homelessId { get; set; }
        public string homelessName { get; set; }
        public string homelessDescription { get; set; }
        public string youtubeURL { get; set; }
        public string analyzeKey { get; set; }
    }
}