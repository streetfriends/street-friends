﻿namespace Model
{
    public class SupportService
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string freeCall { get; set; }
        public string email { get; set; }
        public string url { get; set; }
        public string twitter { get; set; }
        public string socialMedia { get; set; }
        public string monday { get; set; }
        public string tuesday { get; set; }
        public string wednesday { get; set; }
        public string thursday { get; set; }
        public string friday { get; set; }
        public string saturday { get; set; }
        public string sunday { get; set; }
        public string publicHoliday { get; set; }
        public string cost { get; set; }
        public string tramRoute { get; set; }
        public string busRoute { get; set; }
        public string trainStation { get; set; }
        public string category_1 { get; set; }
        public string category_2 { get; set; }
        public string category_3 { get; set; }
        public string category_4 { get; set; }
        public string category_5 { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }
}