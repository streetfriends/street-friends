﻿namespace Model
{
    public class Employment
    {
        public string id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public string email { get; set; }
        public string twitter { get; set; }
    }
}
