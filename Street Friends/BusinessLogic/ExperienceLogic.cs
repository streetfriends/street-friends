﻿using Model;
using DataAccess;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class ExperienceLogic
    {
        public int AddExperienceBL(Experience myExperience)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.AddExperienceDA(myExperience);
            }
            catch
            {
                throw;
            }
        }

        public int DeleteExperienceBL(int experienceId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.DeleteExperienceDA(experienceId);
            }
            catch
            {
                throw;
            }
        }

        public SqlDataAdapter ShowExperienceBL(int userId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.ShowExperienceDA(userId);
            }
            catch
            {
                throw;
            }
        }

        public SqlDataAdapter ShowFavoriteExperienceBL(int userId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.ShowFavoriteExperienceDA(userId);
            }
            catch
            {
                throw;
            }
        }

        public SqlDataAdapter ShowYourExperienceBL(int userId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.ShowYourExperienceDA(userId);
            }
            catch
            {
                throw;
            }
        }

        public int AddFavoriteExperienceBL(int userId, int experienceId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.AddFavoriteExperienceDA(userId, experienceId);
            }
            catch
            {
                throw;
            }
        }

        public bool IsAlreadyFavoriteBL(int userId, int experienceId)
        {
            try
            {
                ExperienceDA experience = new ExperienceDA();
                return experience.IsAlreadyFavoriteDA(userId, experienceId);
            }
            catch
            {
                throw;
            }
        }
    }
}