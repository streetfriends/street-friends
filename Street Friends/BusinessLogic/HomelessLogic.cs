﻿using Model;
using DataAccess;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class HomelessLogic
    {
        public SqlDataAdapter GetAllHomelessBL()
        {
            try
            {
                HomelessDA homeless = new HomelessDA();
                return homeless.GetAllHomelessDA();
            }
            catch
            {
                throw;
            }
        }

        public Homeless GetHomelessByIDBL(int homelessID)
        {
            try
            {
                HomelessDA homeless = new HomelessDA();
                return homeless.GetHomelessByIDDA(homelessID);
            }
            catch
            {
                throw;
            }
        }
    }
}