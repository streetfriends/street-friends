﻿using Model;
using DataAccess;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;

namespace BusinessLogic
{
    public class VolunteerCenterLogic
    {
        public Tuple<SqlDataAdapter, DataTable> GetAllVolunteerCenterBL(int userId)
        {
            try
            {
                VolunteerCenterDA volunteerCenter = new VolunteerCenterDA();
                return volunteerCenter.GetAllVolunteerCenterDA(userId);
            }
            catch
            {
                throw;
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetVolunteerCenterByCategoryBL(int userId, List<string> categoryName)
        {
            try
            {
                VolunteerCenterDA volunteerCenter = new VolunteerCenterDA();
                return volunteerCenter.GetVolunteerCenterByCategoryDA(userId, categoryName);
            }
            catch
            {
                throw;
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetVolunteerByCategoryAndPostCodeBL(int userId, List<string> categories, string postcode)
        {
            try
            {
                VolunteerCenterDA volunteerCenter = new VolunteerCenterDA();
                return volunteerCenter.GetVolunteerByCategoryAndPostCodeDA(userId, categories, postcode);
            }
            catch
            {
                throw;
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetVolunteerByPostCodeBL(int userId, string postcode)
        {
            try
            {
                VolunteerCenterDA volunteerCenter = new VolunteerCenterDA();
                return volunteerCenter.GetVolunteerByPostCodeDA(userId, postcode);
            }
            catch
            {
                throw;
            }
        }
        
        public List<Postcode> GetAllPostCodeBL()
        {
            try
            {
                VolunteerCenterDA volunteerCenter = new VolunteerCenterDA();
                return volunteerCenter.GetAllPostCodeDA();
            }
            catch
            {
                throw;
            }
        }
    }
}