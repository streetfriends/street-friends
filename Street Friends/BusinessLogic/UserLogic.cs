﻿using System;
using Model;
using DataAccess;

namespace BusinessLogic
{
    public class UserLogic
    {
        public bool RegisterUserBL(User myUser)
        {
            try
            {
                UserDA user = new UserDA();
                return user.RegisterUserDA(myUser);
            }
            catch
            {
                throw;
            }
        }

        public User GetUserDetailsByIDBL(int userId)
        {
            try
            {
                UserDA user = new UserDA();
                return user.GetUserDetailsByIDDA(userId);
            }
            catch
            {
                throw;
            }
        }

        public String[] AuthenticateBL(string email, string password)
        {
            try
            {
                UserDA user = new UserDA();
                return user.AuthenticateDA(email, password);
            }
            catch
            {
                throw;
            }
        }

        public int UpdatePasswordBL(int userId, string password)
        {
            try
            {
                UserDA user = new UserDA();
                return user.UpdatePasswordDA(userId, password);
            }
            catch
            {
                throw;
            }
        }
    }
}