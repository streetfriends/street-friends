﻿using Model;
using DataAccess;
using System.Data;
using System.Data.SqlClient;
using System;

namespace BusinessLogic
{
    public class SupportServiceLogic
    {
        public Tuple<SqlDataAdapter, DataTable> GetAllSupportServiceBL()
        {
            try
            {
                SupportServiceDA supportService = new SupportServiceDA();
                return supportService.GetAllSupportServiceDA();
            }
            catch
            {
                throw;
            }
        }

        public SupportService GetSupportServiceByIDBL(int supportServiceID)
        {
            try
            {
                SupportServiceDA supportService = new SupportServiceDA();
                return supportService.GetSupportServiceByIDDA(supportServiceID);
            }
            catch
            {
                throw;
            }
        }
    }
}