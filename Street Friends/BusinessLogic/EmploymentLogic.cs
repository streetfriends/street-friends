﻿using Model;
using DataAccess;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class EmploymentLogic
    {
        public SqlDataAdapter GetAllEmploymentServiceBL()
        {
            try
            {
                EmploymentDA employment = new EmploymentDA();
                return employment.GetAllEmploymentServiceDA();
            }
            catch
            {
                throw;
            }
        }

        public Employment GetEmploymentServiceBySiteCodeBL(string siteCode)
        {
            try
            {
                EmploymentDA employment = new EmploymentDA();
                return employment.GetEmploymentServiceBySiteCodeDA(siteCode);
            }
            catch
            {
                throw;
            }
        }
    }
}
