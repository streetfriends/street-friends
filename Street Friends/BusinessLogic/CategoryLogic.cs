﻿using Model;
using DataAccess;
using System.Collections.Generic;

namespace BusinessLogic
{
    public class CategoryLogic
    {
        public List<Category> GetAllCategoriesBL()
        {
            try
            {
                CategoryDA category = new CategoryDA();
                return category.GetAllCategoriesDA();
            }
            catch
            {
                throw;
            }
        }

        public string GetCategoryByIDBL(int categoryID)
        {
            try
            {
                CategoryDA category = new CategoryDA();
                return category.GetCategoryByIDDA(categoryID);
            }
            catch
            {
                throw;
            }
        }
    }
}