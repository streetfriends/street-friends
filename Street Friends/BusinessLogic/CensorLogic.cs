﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;

namespace BusinessLogic
{
    public class CensorLogic
    {
        public IList<string> CensoredWords { get; private set; }

        public CensorLogic()
        {
            List<string> words = new List<string>();
            XmlDocument xmlDoc = new XmlDocument();
            

            string query = "/WordList/word";

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "Assets//bannedword//WordList.xml";
            xmlDoc.Load(path);
            
            foreach (XmlNode node in xmlDoc.SelectNodes(query))
            {
                words.Add(node.ChildNodes[0].InnerText);
            }
            
            CensoredWords = new List<string>(words);
        }

        public int CensorText(string text)
        {
            if (text == null)
                throw new ArgumentNullException("text");

            string censoredText = text;

            foreach (string censoredWord in CensoredWords)
            {
                string regularExpression = ToRegexPattern(censoredWord);

                censoredText = Regex.Replace(censoredText, regularExpression, StarCensoredMatch,
                  RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
            }

            if (text != censoredText)
                return -1;
            else
                return 1;
        }

        private static string StarCensoredMatch(Match m)
        {
            string word = m.Captures[0].Value;

            return new string('*', word.Length);
        }

        private string ToRegexPattern(string wildcardSearch)
        {
            string regexPattern = Regex.Escape(wildcardSearch);

            regexPattern = regexPattern.Replace(@"\*", ".*?");
            regexPattern = regexPattern.Replace(@"\?", ".");

            if (regexPattern.StartsWith(".*?"))
            {
                regexPattern = regexPattern.Substring(3);
                regexPattern = @"(^\b)*?" + regexPattern;
            }

            regexPattern = @"\b" + regexPattern + @"\b";

            return regexPattern;
        }
    }
}