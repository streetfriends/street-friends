﻿using DataAccess;

namespace BusinessLogic
{
    public class RatingLogic
    {
        public bool DoesRatingExistBL(int userId, int volunteerId)
        {
            try
            {
                RatingDA rating = new RatingDA();
                return rating.DoesRatingExistDA(userId, volunteerId);
            }
            catch
            {
                throw;
            }
        }

        public int AddRatingBL(int userId, int volunteerId, int rating)
        {
            try
            {
                RatingDA myRating = new RatingDA();
                return myRating.AddRatingDA(userId, volunteerId, rating);
            }
            catch
            {
                throw;
            }
        }

        public int UpdateRatingBL(int userId, int volunteerId, int rating)
        {
            try
            {
                RatingDA myRating = new RatingDA();
                return myRating.UpdateRatingDA(userId, volunteerId, rating);
            }
            catch
            {
                throw;
            }
        }
    }
}