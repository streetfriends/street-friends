﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Model;
using System;

namespace DataAccess
{
    public class UserDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public bool RegisterUserDA(User myUser)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_RegisterUser", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FullName", myUser.fullname);
                cmd.Parameters.AddWithValue("@Email", myUser.email);
                cmd.Parameters.AddWithValue("@Password", myUser.password);

                con.Open();

                cmd.ExecuteNonQuery();
                cmd.Dispose();
                return true;
            }
            catch
            {
                return false;
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public User GetUserDetailsByIDDA(int userId)
        {
            User myUser = new User();
            
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetUserDetailsByID", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@UserId", userId);
                
                con.Open();

                SqlDataReader da = cmd.ExecuteReader();
                da.Read();

                if (da.HasRows)
                {
                    myUser.userId = int.Parse(da[0].ToString());
                    myUser.fullname = da[1].ToString();
                    myUser.email = da[2].ToString();
                    myUser.password = da[3].ToString();
                }

                return myUser;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public int UpdatePasswordDA(int userId, string password)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UpdatePassword", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@Password", password);

                con.Open();
                return cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public String[] AuthenticateDA(string email, string password)
        {
            String[] Info = new String[2];

            try
            {
                SqlCommand cmd = new SqlCommand("sp_Authenticate", con);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Email", email);
                cmd.Parameters.AddWithValue("@Password", password);

                con.Open();

                SqlDataReader da = cmd.ExecuteReader();
                da.Read();

                if (da.HasRows)
                {
                    Info[0] = da[0].ToString();
                    Info[1] = da[1].ToString();
                }
                else
                {
                    Info[0] = "";
                    Info[1] = "";
                }

                return Info;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}