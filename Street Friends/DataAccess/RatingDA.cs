﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    public class RatingDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public bool DoesRatingExistDA(int userId, int volunteerId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_DoesRatingExist", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@VolunteerId", volunteerId);

                con.Open();

                SqlDataReader da = cmd.ExecuteReader();
                da.Read();

                if (da.HasRows)
                    return true;
                else
                    return false;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public int AddRatingDA(int userId, int volunteerId, int rating)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_AddRating", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@VolunteerId", volunteerId);
                cmd.Parameters.AddWithValue("@Rating", rating);

                con.Open();

                int Result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return Result;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public int UpdateRatingDA(int userId, int volunteerId, int rating)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_UpdateRating", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@VolunteerId", volunteerId);
                cmd.Parameters.AddWithValue("@Rating", rating);

                con.Open();

                int Result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return Result;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}