﻿using System;
using Model;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace DataAccess
{
    public class VolunteerCenterDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
        
        public Tuple<SqlDataAdapter, DataTable> GetAllVolunteerCenterDA(int userId)
        {
            try
            {
                
                SqlCommand cmd = new SqlCommand("sp_GetAllVolunteer", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);

                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                Tuple<SqlDataAdapter, DataTable> allVolunteerCenters = new Tuple<SqlDataAdapter, DataTable>(dataAdapter, dataTable);

                return allVolunteerCenters;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetVolunteerByCategoryAndPostCodeDA(int userId, List<string> selectedCategories, string postcode)
        {
            string category = "'";

            for (int i = 0; i < selectedCategories.Count; i++)
            {
                category += selectedCategories[i] + "'" + "," + "'";
            }

            category += "None'";

            string command = "Select Distinct * From (Select v.volunteer_id, v.volunteer_name, v.volunteer_description, v.volunteer_address, v.volunteer_phone, v.volunteer_email, v.volunteer_website, v.volunteer_twitter, v.volunteer_latitude, v.volunteer_longitude, RIGHT(cat.categories, LEN(cat.categories) -1) as category_list, (Select ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 2),0) From VolunteerRating Where volunteer_id = V.volunteer_id) as rating, (Select COUNT(rating_id) From VolunteerRating Where volunteer_id = V.volunteer_id) as votes, (Select CASE WHEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = "+ userId + ") > 0 THEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = "+ userId +") ELSE 0 END) as yourrating From Volunteer v LEFT JOIN VolunteerRating vr ON vr.volunteer_id = v.volunteer_id INNER JOIN ( Select distinct t1.volunteer_id, STUFF( ( Select distinct ', ' + t2.category_name From ( Select v.volunteer_id, c.category_name From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id INNER JOIN Category c ON c.category_id = vc.category_id ) t2 Where t1.volunteer_id = t2.volunteer_id FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)'), 1, 0, '') categories From ( Select v.volunteer_id, c.category_name From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id INNER JOIN Category c ON c.category_id = vc.category_id Where c.category_name In ( " + category + " )) t1 ) cat ON v.volunteer_id = cat.volunteer_id ) as tbl Where volunteer_address like '%" + postcode + "%'";
            
            try
            {
                SqlCommand cmd = new SqlCommand(command, con);
                
                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                Tuple<SqlDataAdapter, DataTable> allVolunteerCenters = new Tuple<SqlDataAdapter, DataTable>(dataAdapter, dataTable);

                return allVolunteerCenters;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        
        public Tuple<SqlDataAdapter, DataTable> GetVolunteerByPostCodeDA(int userId, string postcode)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetVolunteerByPostCode", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@PostCode", "%" + postcode + "%");

                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                Tuple<SqlDataAdapter, DataTable> allVolunteerCenters = new Tuple<SqlDataAdapter, DataTable>(dataAdapter, dataTable);

                return allVolunteerCenters;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Postcode> GetAllPostCodeDA()
        {
            List<Postcode> allSuburb = new List<Postcode>();

            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetAllPostcode", con);

                cmd.CommandType = CommandType.StoredProcedure;
                
                con.Open();

                SqlDataReader dataReader = cmd.ExecuteReader();
                while (dataReader.Read())
                {
                    Postcode postcode = new Postcode();
                    postcode.postcode_id = int.Parse(dataReader[0].ToString());
                    postcode.postcode_name = dataReader[1].ToString();
                    
                    allSuburb.Add(postcode);
                }

                return allSuburb;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetVolunteerCenterByCategoryDA(int userId, List<string> selectedCategories)
        {
            string category = "'";

            for (int i = 0; i < selectedCategories.Count; i++)
            {
                category += selectedCategories[i] + "'" + "," + "'";
            }

            category += "None'";

            string command = "Select Distinct * From (Select v.volunteer_id, v.volunteer_name, v.volunteer_description, v.volunteer_address, v.volunteer_phone, v.volunteer_email, v.volunteer_website, v.volunteer_twitter, v.volunteer_latitude, v.volunteer_longitude, RIGHT(cat.categories, LEN(cat.categories) -1) as category_list, (Select ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 2),0) From VolunteerRating Where volunteer_id = V.volunteer_id) as rating, (Select COUNT(rating_id) From VolunteerRating Where volunteer_id = V.volunteer_id) as votes, (Select CASE WHEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = "+ userId +") > 0 THEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = "+ userId +") ELSE 0 END) as yourrating  From Volunteer v LEFT JOIN VolunteerRating vr ON vr.volunteer_id = v.volunteer_id INNER JOIN ( Select distinct t1.volunteer_id, STUFF( ( Select distinct ', ' + t2.category_name From ( Select v.volunteer_id, c.category_name From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id INNER JOIN Category c ON c.category_id = vc.category_id ) t2 Where t1.volunteer_id = t2.volunteer_id FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)'), 1, 0, '') categories From ( Select v.volunteer_id, c.category_name From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id INNER JOIN Category c ON c.category_id = vc.category_id Where c.category_name In ( " + category + " )) t1 ) cat ON v.volunteer_id = cat.volunteer_id ) as tbl";
            
            try
            {
                SqlCommand cmd = new SqlCommand(command, con);
                
                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                Tuple<SqlDataAdapter, DataTable> allVolunteerCenters = new Tuple<SqlDataAdapter, DataTable>(dataAdapter, dataTable);

                return allVolunteerCenters;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}