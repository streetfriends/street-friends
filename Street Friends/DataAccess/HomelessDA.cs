﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Model;

namespace DataAccess
{
    public class HomelessDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public SqlDataAdapter GetAllHomelessDA()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetAllHomeless", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                return sda;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public Homeless GetHomelessByIDDA(int homelessID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetHomelessByID", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HomelessID", homelessID);

                con.Open();

                SqlDataReader dataReader = cmd.ExecuteReader();
                dataReader.Read();

                Homeless homeless = new Homeless();
                homeless.homelessId = int.Parse(dataReader[0].ToString());
                homeless.homelessName = dataReader[1].ToString();
                homeless.homelessDescription = dataReader[2].ToString();
                homeless.youtubeURL = dataReader[3].ToString();
                homeless.analyzeKey = dataReader[5].ToString();

                return homeless;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}