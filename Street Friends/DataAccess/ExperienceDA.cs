﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Model;

namespace DataAccess
{
    public class ExperienceDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public int AddExperienceDA(Experience myExperience)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ShareExperience", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", myExperience.userId);
                cmd.Parameters.AddWithValue("@Description", myExperience.postDescription);
                cmd.Parameters.AddWithValue("@PostDate", myExperience.postDate);
                cmd.Parameters.AddWithValue("@PostTime", myExperience.postTime);
                
                con.Open();

                int Result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return Result;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public int DeleteExperienceDA(int experienceId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_DeleteExperience", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ExperienceId", experienceId);

                con.Open();

                int Result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return Result;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public int AddFavoriteExperienceDA(int userId, int experienceId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_AddFavoriteExperience", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@ExperienceId", experienceId);

                con.Open();

                int Result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                return Result;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public bool IsAlreadyFavoriteDA(int userId, int experienceId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_IsAlreadyFavorite", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);
                cmd.Parameters.AddWithValue("@ExperienceId", experienceId);

                con.Open();

                SqlDataReader da = cmd.ExecuteReader();
                da.Read();

                if (da.HasRows)
                    return true;
                else
                    return false;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public SqlDataAdapter ShowExperienceDA(int userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ShowExperience", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);

                con.Open();

                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                return sda;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public SqlDataAdapter ShowYourExperienceDA(int userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ShowYourExperience", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);

                con.Open();

                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                return sda;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public SqlDataAdapter ShowFavoriteExperienceDA(int userId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_ShowFavoriteExperience", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", userId);

                con.Open();

                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                return sda;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}