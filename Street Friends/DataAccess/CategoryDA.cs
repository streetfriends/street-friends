﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Model;
using System.Collections.Generic;

namespace DataAccess
{
    public class CategoryDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public List<Category> GetAllCategoriesDA()
        {
            List<Category> allCategories = new List<Category>();

            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetAllCategories", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                
                SqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    Category category = new Category();
                    category.id = int.Parse(dataReader[0].ToString());
                    category.name = dataReader[1].ToString();

                    allCategories.Add(category);
                }

                return allCategories;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public string GetCategoryByIDDA(int categoryId)
        {
            string categoryName = "Unknown";

            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetCategoryByID", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CategoryId", categoryId);

                con.Open();

                SqlDataReader da = cmd.ExecuteReader();
                da.Read();

                if (da.HasRows)
                {
                    categoryName = da[0].ToString();
                }
                
                return categoryName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}