﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Model;
using System.Collections.Generic;

namespace DataAccess
{
    public class SupportServiceDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public List<SupportService> GetAllSupportServiceDA_1()
        {
            try
            {
                List<SupportService> allSupportService = new List<SupportService>();

                SqlCommand cmd = new SqlCommand("sp_GetAllSupportService", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                SqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    SupportService supportService = new SupportService();
                    supportService.id = int.Parse(dataReader[0].ToString());
                    supportService.name = dataReader[1].ToString();
                    supportService.address = dataReader[2].ToString() + ", " + dataReader[3].ToString();
                    supportService.phone = dataReader[4].ToString();
                    supportService.freeCall = dataReader[5].ToString();
                    supportService.email = dataReader[6].ToString();
                    supportService.url = dataReader[7].ToString();
                    supportService.twitter = dataReader[8].ToString();
                    supportService.socialMedia = dataReader[9].ToString();
                    supportService.monday = dataReader[10].ToString();
                    supportService.tuesday = dataReader[11].ToString();
                    supportService.wednesday = dataReader[12].ToString();
                    supportService.thursday = dataReader[13].ToString();
                    supportService.friday = dataReader[14].ToString();
                    supportService.saturday = dataReader[15].ToString();
                    supportService.sunday = dataReader[16].ToString();
                    supportService.publicHoliday = dataReader[17].ToString();
                    supportService.cost = dataReader[18].ToString();
                    supportService.tramRoute = dataReader[19].ToString();
                    supportService.busRoute = dataReader[20].ToString();
                    supportService.trainStation = dataReader[21].ToString();
                    supportService.category_1 = dataReader[22].ToString();
                    supportService.category_2 = dataReader[23].ToString();
                    supportService.category_3 = dataReader[24].ToString();
                    supportService.category_4 = dataReader[25].ToString();
                    supportService.category_5 = dataReader[26].ToString();
                    supportService.longitude = dataReader[27].ToString();
                    supportService.latitude = dataReader[28].ToString();
                    
                    allSupportService.Add(supportService);
                }

                return allSupportService;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public Tuple<SqlDataAdapter, DataTable> GetAllSupportServiceDA()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetAllSupportService", con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();

                dataAdapter.Fill(dataTable);

                Tuple<SqlDataAdapter, DataTable> allVolunteerCenters = new Tuple<SqlDataAdapter, DataTable>(dataAdapter, dataTable);

                return allVolunteerCenters;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
        
        public SupportService GetSupportServiceByIDDA(int supportServiceID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetSupportServiceByID", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SupportServiceID", supportServiceID);

                con.Open();

                SqlDataReader dataReader = cmd.ExecuteReader();
                dataReader.Read();

                SupportService supportService = new SupportService();
                supportService.id = int.Parse(dataReader[0].ToString());
                supportService.name = dataReader[1].ToString();

                if (dataReader[2].ToString() != "")
                    supportService.address = dataReader[2].ToString() + ", " + dataReader[3].ToString();
                else
                    supportService.address = "";

                supportService.phone = dataReader[4].ToString();
                supportService.freeCall = dataReader[5].ToString();
                supportService.email = dataReader[6].ToString();
                supportService.url = dataReader[7].ToString();
                supportService.twitter = dataReader[8].ToString();
                supportService.socialMedia = dataReader[9].ToString();
                supportService.monday = dataReader[10].ToString();
                supportService.tuesday = dataReader[11].ToString();
                supportService.wednesday = dataReader[12].ToString();
                supportService.thursday = dataReader[13].ToString();
                supportService.friday = dataReader[14].ToString();
                supportService.saturday = dataReader[15].ToString();
                supportService.sunday = dataReader[16].ToString();
                supportService.publicHoliday = dataReader[17].ToString();
                supportService.cost = dataReader[18].ToString();
                supportService.tramRoute = dataReader[19].ToString();
                supportService.busRoute = dataReader[20].ToString();
                supportService.trainStation = dataReader[21].ToString();
                supportService.category_1 = dataReader[22].ToString();
                supportService.category_2 = dataReader[23].ToString();
                supportService.category_3 = dataReader[24].ToString();
                supportService.category_4 = dataReader[25].ToString();
                supportService.category_5 = dataReader[26].ToString();
                supportService.longitude = dataReader[27].ToString();
                supportService.latitude = dataReader[28].ToString();
                
                return supportService;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }
    }
}