﻿using Model;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace DataAccess
{
    public class EmploymentDA
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());

        public SqlDataAdapter GetAllEmploymentServiceDA()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("sp_GetAllEmploymentService", con);

                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();

                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                return sda;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public Employment GetEmploymentServiceBySiteCodeDA(string siteCode)
        {
            Employment employment = new Employment();
            return employment;

            /*try
            {
                SqlCommand cmd = new SqlCommand("sp_GetServiceProviderBySiteCode", con);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SiteCode", siteCode);

                con.Open();

                SqlDataReader dataReader = cmd.ExecuteReader();
                dataReader.Read();

                Employment employment = new Employment();
                employment.siteName = dataReader[0].ToString();
                employment.siteAddress = dataReader[1].ToString() + ", " + dataReader[2].ToString() + ", " + dataReader[3].ToString() + " " + dataReader[4].ToString();
                employment.url = dataReader[5].ToString();
                employment.email = dataReader[6].ToString();
                employment.phoneNo = dataReader[7].ToString();
                employment.longitude = dataReader[8].ToString();
                employment.latitude = dataReader[9].ToString();

                return employment;
            }
            catch
            {
                throw;
            }
            finally
            {
                con.Close();
            }*/
        }


    }
}
