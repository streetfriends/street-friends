Create Database StreetFriends;

GO

USE StreetFriends;

GO

CREATE TABLE EmploymentService
(
	employment_id int IDENTITY PRIMARY KEY,
	employment_name varchar(255) NOT NULL,
	employment_desc varchar(MAX) NOT NULL,
	employment_address varchar(255) NOT NULL,
	employment_phone varchar(20) NULL,
	employment_website varchar(50) NULL,
	employment_email varchar(50) NULL,
	employment_twitter varchar(50) NULL,
)

Go

CREATE TABLE Homeless
(
	homeless_id int IDENTITY PRIMARY KEY,
	homeless_name varchar(50) NOT NULL,
	homeless_description varchar(MAX) NULL,
	youtube_url varchar(50) NOT NULL,
	homeless_image varchar(50) NOT NULL,
	analyze_key varchar(20) NULL
)

Go

INSERT INTO Homeless VALUES('Shana','When she was 8 years old, her mother died because of overdoes prescribed sleeping pills. Her dad kicked her out when she was 16 years old. After that she lived in her friend�s home and aunt�s home, but she became homeless finally. Melbourne City Mission has helped her a lot of things, such as live place, food and let she back to school. Now she study teaching for special education.', 'https://www.youtube.com/embed/q9SH55_-8yg', '../Assets/homeless/Shana.jpg', '74aed7495c');
INSERT INTO Homeless VALUES('Erik','The person left home when he was 16 years old, he is a transgender person who are struggling with a lot of verbal and emotional abuse. he found an organisation which provide health care and life skills programs to him, which is really helpful. Now he has started going to university for bachelor of arts.', 'https://www.youtube.com/embed/5n9_PTUu9iw?ecver=2', '../Assets/homeless/Erik.jpg', 'c51ba0d2a4');
INSERT INTO Homeless VALUES('Stephanie','Stephanie was medically discharged from the Royal Army Medical Corps after being injured in Afghanistan. She now sleeps rough on the cold streets of Central London. Stephanie says you lose everything on the streets and no one seems to care.', 'https://www.youtube.com/embed/XpefSx1IkYE?ecver=2', '../Assets/homeless/Stephanie.jpg', '0423c74955');
INSERT INTO Homeless VALUES('Leonard','In this great country of ours, there are homeless people that sleep in a park directly across the street from the White House. The irony of that I hope is disturbing to more people than just me. At least for Leonard, he has found a purpose.', 'https://www.youtube.com/embed/wUJZQWTVNBY?ecver=2', '../Assets/homeless/Leonard.jpg', '4815e24908');
INSERT INTO Homeless VALUES('Kelly','Kelly approached me at a forum about fighting the increase in the criminalization of homelessness this week. She walked up and asked, �are you Mark of Invisible People?� Kelly then asked if I can help her tell her story. I didn�t think she was currently homeless so I responded that Invisible People empowers people still in some state of homelessness to share their stories. Kelly then enlightened me that not only is she still homeless she is sleeping outside in an alley.', 'https://www.youtube.com/embed/dDVluhJ-uCY?ecver=2', '../Assets/homeless/Kelly.jpg', '056d1f6d0f');
INSERT INTO Homeless VALUES('Percy','Percy�s family moved out of Chicago. He decided to stay and see if he can make it on his own but ended up homeless. Percy has been on the streets for two years. He just passed the drug test to get into housing!', 'https://www.youtube.com/embed/UPg1Kz7uGpE?ecver=2', '../Assets/homeless/Percy.jpg', '723173f76e');
INSERT INTO Homeless VALUES('Roman','It was an extremely hot day in Washington D.C. You could not miss Roman sitting on the sidewalk with close to a dozen full bottles of water in front of him. What�s interesting is even though it was very obvious Roman had plenty of water, people still kept putting bottles of water at his feet and not the worm medicine he needed to help his dog!', 'https://www.youtube.com/embed/9zYjiNEmmDE?ecver=2', '../Assets/homeless/Roman.jpg', 'f54ffd3c6e');
INSERT INTO Homeless VALUES('Jackie','Jackie is one amazing woman. She has a story that makes you want to jump for joy and cry at the same time. Jackie has lived in the streets for over a decade, but she retains a positive glow and energy about her. Speaking with Jackie, you cannot help but smile with her. You almost forget she is sleeping outside!', 'https://www.youtube.com/embed/XcHL58IdQZA?ecver=2', '../Assets/homeless/Jackie.jpg', '66ad790e17');

Go

CREATE TABLE Volunteer
(
	volunteer_id int IDENTITY PRIMARY KEY,
	volunteer_name varchar(50) NOT NULL,
	volunteer_description varchar(MAX) NOT NULL,
	volunteer_address varchar(100) NOT NULL,
	volunteer_email varchar(50) NULL,
	volunteer_phone varchar(20) NULL,
	volunteer_website varchar(50) NOT NULL,
	volunteer_twitter varchar(20),
	volunteer_latitude float NOT NULL,
	volunteer_longitude float NOT NULL
)

Go

CREATE TABLE [SupportService]
(
	[supportservice_id] int IDENTITY PRIMARY KEY,
	[name] [varchar](255) NULL,
	[address] [varchar](255) NULL,
	[suburb] [varchar](255) NULL,
	[phone] [varchar](255) NULL,
	[free_call] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[website] [varchar](255) NULL,
	[twitter] [varchar](255) NULL,
	[social_media] [varchar](255) NULL,
	[monday] [varchar](255) NULL,
	[tuesday] [varchar](255) NULL,
	[wednesday] [varchar](255) NULL,
	[thursday] [varchar](255) NULL,
	[friday] [varchar](255) NULL,
	[saturday] [varchar](255) NULL,
	[sunday] [varchar](255) NULL,
	[public_holidays] [varchar](255) NULL,
	[cost] [varchar](255) NULL,
	[tram_routes] [varchar](255) NULL,
	[bus_routes] [varchar](255) NULL,
	[nearest_train_station] [varchar](255) NULL,
	[category_1] [varchar](255) NULL,
	[category_2] [varchar](255) NULL,
	[category_3] [varchar](255) NULL,
	[category_4] [varchar](255) NULL,
	[category_5] [varchar](255) NULL,
	[longitude] float NULL,
	[latitude] float NULL
)

Go

CREATE TABLE PostCode
(
	postcode_id int IDENTITY PRIMARY KEY,
	postcode_name varchar(100) NOT NULL
)

Go

Insert Into PostCode(postcode_name) VALUES ('3205')
Insert Into PostCode(postcode_name) VALUES ('3000')
Insert Into PostCode(postcode_name) VALUES ('3182')
Insert Into PostCode(postcode_name) VALUES ('3121')
Insert Into PostCode(postcode_name) VALUES ('3066')
Insert Into PostCode(postcode_name) VALUES ('3197')
Insert Into PostCode(postcode_name) VALUES ('3140')


Go

CREATE TABLE Category
(
	category_id int IDENTITY PRIMARY KEY,
	category_name varchar(50) NOT NULL
)

Go

INSERT INTO Category VALUES('Accomodation');
INSERT INTO Category VALUES('Personal Planning & Support');
INSERT INTO Category VALUES('Training');
INSERT INTO Category VALUES('Health');
INSERT INTO Category VALUES('Alcohol / Drugs');
INSERT INTO Category VALUES('Employment');
INSERT INTO Category VALUES('Food');
INSERT INTO Category VALUES('Women');
INSERT INTO Category VALUES('Financial');
INSERT INTO Category VALUES('Children');
INSERT INTO Category VALUES('Cloths');
INSERT INTO Category VALUES('Legal');

Go

CREATE TABLE VolunteerCategory
(
	[category_id] [int] NULL,
	[volunteer_id] [int] NULL,
	FOREIGN KEY ([category_id]) REFERENCES [Category]([category_id]),
	FOREIGN KEY ([volunteer_id]) REFERENCES [Volunteer]([volunteer_id])
) 

Go

INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 1)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 2)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 3)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 4)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 5)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (1, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (2, 1)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (2, 5)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (2, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (2, 8)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (2, 9)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (3, 1)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (3, 4)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 2)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 3)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 4)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (4, 8)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (5, 2)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (5, 5)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (5, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (6, 2)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (7, 3)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (7, 5)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (7, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (7, 8)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (7, 9)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (8, 3)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (8, 4)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (8, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (9, 3)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (9, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (10, 4)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (10, 7)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (11, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (12, 6)
INSERT [VolunteerCategory] ([category_id], [volunteer_id]) VALUES (12, 7)

GO

CREATE TABLE [User]
(
	[user_id] int IDENTITY PRIMARY KEY,
	user_fullname varchar(50) NOT NULL,
	user_email varchar(50) NOT NULL,
	user_password varchar(50) NOT NULL,
)

Go

CREATE TABLE Experience
(
	experience_id int IDENTITY PRIMARY KEY,
	[description] varchar(MAX) NOT NULL,
	[date] varchar(50) NOT NULL,
	[time] varchar(50) NOT NULL,
	isapproved bit NOT NULL,
	[user_id] int NOT NULL,
	FOREIGN KEY ([user_id]) REFERENCES [User]([user_id])
)

Go

CREATE TABLE FavoriteExperience
(
	fav_id int IDENTITY PRIMARY KEY,
	[user_id] int NOT NULL,
	[experience_id] int NOT NULL,
	FOREIGN KEY ([user_id]) REFERENCES [User]([user_id]),
	FOREIGN KEY ([experience_id]) REFERENCES [Experience]([experience_id])
)

Go

CREATE TABLE VolunteerRating
(
	rating_id int IDENTITY PRIMARY KEY,
	[user_id] int NOT NULL,
	[volunteer_id] int NOT NULL,
	rating int NOT NULL,
	FOREIGN KEY ([user_id]) REFERENCES [User]([user_id]),
	FOREIGN KEY ([volunteer_id]) REFERENCES Volunteer([volunteer_id]),
)

Go

-- Stored Procedure --

Create Procedure sp_GetAllEmploymentService
AS
Begin
	Select * From EmploymentService
	Order By employment_name
End

Go

Create Procedure sp_GetServiceProviderBySiteCode
@SiteCode nvarchar(4)
AS
Begin
	Select site_name, site_address, location_name, site_state, site_postcode, site_url, site_email, phone_number, longitude, latitude
	From [ServiceProvider]
	Where site_code = @SiteCode
End

Go

Create Procedure sp_GetAllSupportService
AS
Begin
	Select * From [SupportService] 
	Order By [name], supportservice_id
End

Go

Create Procedure sp_GetSupportServiceByID
@SupportServiceID int
AS
Begin
	Select * From SupportService Where supportservice_id = @SupportServiceID
End

Go

Create Procedure sp_GetHomelessByID
@HomelessID int
AS
Begin
	Select * From Homeless Where homeless_id = @HomelessID
End

Go

Create Procedure sp_GetAllHomeless
AS
Begin
	Select * From Homeless
End

Go

Create Procedure sp_GetAllVolunteer
@UserId int
AS
Begin
	Select Distinct v.volunteer_id, v.volunteer_name, v.volunteer_description, v.volunteer_address, v.volunteer_phone, v.volunteer_email, v.volunteer_website, v.volunteer_twitter, v.volunteer_latitude, v.volunteer_longitude, RIGHT(cat.categories, LEN(cat.categories) -1) as category_list, (Select ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 2),0) From VolunteerRating Where volunteer_id = V.volunteer_id) as rating, (Select COUNT(rating_id) From VolunteerRating Where volunteer_id = V.volunteer_id) as votes, (Select CASE WHEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = @UserId) > 0 THEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = @UserId) ELSE 0 END) as yourrating
	From Volunteer v LEFT JOIN VolunteerRating vr ON vr.volunteer_id = v.volunteer_id INNER JOIN
	(
		Select distinct t1.volunteer_id, STUFF(
		(
			Select distinct ', ' + t2.category_name
			From 
			( 
				Select v.volunteer_id, c.category_name 
				From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id 
				INNER JOIN Category c ON c.category_id = vc.category_id) t2
				Where t1.volunteer_id = t2.volunteer_id
				FOR XML PATH(''), TYPE
			).value('.', 'NVARCHAR(MAX)'), 1, 0, '') categories
		From (
			Select v.volunteer_id, c.category_name 
			From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id 
			INNER JOIN Category c ON c.category_id = vc.category_id) t1
		) cat
	ON v.volunteer_id = cat.volunteer_id;
End

Go

Create Procedure sp_GetAllCategories
AS
Begin
	Select * From Category
End

Go

Create Procedure sp_GetCategoryById
@CategoryId int
AS
Begin
	Select category_name From Category Where category_id = @CategoryId
End

Go

Create Procedure sp_RegisterUser
@FullName varchar(50),
@Email varchar(50),
@Password varchar(50)
AS
Begin
	Insert Into [User](user_fullname, user_email, user_password) VALUES(@FullName, @Email, @Password) 
End

Go

Create Procedure sp_Authenticate
@Email varchar(50),
@Password varchar(50)
AS
Begin
	Select * From [User] Where user_email = @Email AND user_password = @Password
End

Go

Create Procedure sp_ShareExperience
@UserId int,
@Description varchar(Max), 
@PostDate varchar(50), 
@PostTime varchar(50)
AS
Begin
	Insert Into Experience([user_id],[description],[date],[time],[isapproved]) VALUES (@UserId, @Description, @PostDate, @PostTime, 0) 
End 

Go

Create Procedure sp_ShowExperience
@UserId int
AS
Begin
	Select U.user_fullname , E.[description], E.[date], E.[time], U.[user_id], E.experience_id, (Select CASE When (Select fav_id From FavoriteExperience Where [user_id] = @UserId AND experience_id = E.experience_id) IS NULL THEN 0 ELSE 1 END) as isFavorite
	From Experience E, [User] U
	Where E.[user_id] = U.[user_id] AND E.[isapproved] = 1 
	Order By E.experience_id DESC
End

Go

Create Procedure sp_ShowYourExperience
@UserId int
AS
Begin
	Select E.experience_id, U.user_fullname , E.[description], E.[date], E.[time]
	From Experience E, [User] U
	Where E.[user_id] = U.[user_id] AND U.[user_id] = @UserId 
	Order By E.experience_id DESC
End

Go

Create Procedure sp_GetAllPostCode
AS
Begin
	Select * From PostCode
End

Go

Create Procedure sp_GetUserDetailsByID
@UserId int
AS
Begin
	Select * From [User] Where [user_id] = @UserId
End

Go

Create Procedure sp_UpdatePassword
@UserId int, 
@Password varchar(50)
AS
Begin
	Update [User] Set user_password = @Password Where [user_id] = @UserId
End

Go

Create Procedure sp_DeleteExperience
@ExperienceId int
AS
Begin
	Delete From Experience Where experience_id = @ExperienceId
End

Go

Create Procedure sp_AddFavoriteExperience
@UserId int,
@ExperienceId int
AS
Begin
	Insert Into FavoriteExperience Values(@UserId, @ExperienceId)
End

Go

Create Procedure sp_IsAlreadyFavorite
@UserId int,
@ExperienceId int
AS
Begin
	Select * From FavoriteExperience Where [user_id] = @UserId AND experience_id= @ExperienceId
End

Go

Create Procedure sp_ShowFavoriteExperience
@UserId int
AS
Begin
	Select E.experience_id, U.user_fullname , E.[description], E.[date], E.[time]
	From Experience E, [User] U
	Where E.[user_id] = U.[user_id] AND E.experience_id IN (Select experience_id From FavoriteExperience Where [user_id] = @UserId)
	Order By E.experience_id DESC
End

Go

Create Procedure sp_GetVolunteerByPostCode
@UserId int,
@PostCode varchar(50)
AS
Begin
	Select * From
	(
		Select Distinct v.volunteer_id, v.volunteer_name, v.volunteer_description, v.volunteer_address, v.volunteer_phone, v.volunteer_email, v.volunteer_website, v.volunteer_twitter, v.volunteer_latitude, v.volunteer_longitude, RIGHT(cat.categories, LEN(cat.categories) -1) as category_list, (Select ISNULL(ROUND(AVG(CAST(rating AS FLOAT)), 2),0) From VolunteerRating Where volunteer_id = V.volunteer_id) as rating, (Select COUNT(rating_id) From VolunteerRating Where volunteer_id = V.volunteer_id) as votes, (Select CASE WHEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = @UserId) > 0 THEN (Select ISNULL(rating,0) From VolunteerRating Where volunteer_id = V.volunteer_id AND [user_id] = @UserId) ELSE 0 END) as yourrating
		From Volunteer v LEFT JOIN VolunteerRating vr ON vr.volunteer_id = v.volunteer_id INNER JOIN
		(
			Select distinct t1.volunteer_id, STUFF(
			(
				Select distinct ', ' + t2.category_name
				From 
				( 
					Select v.volunteer_id, c.category_name 
					From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id 
					INNER JOIN Category c ON c.category_id = vc.category_id
				) t2
				Where t1.volunteer_id = t2.volunteer_id
				FOR XML PATH(''), TYPE
			).value('.', 'NVARCHAR(MAX)'), 1, 0, '') categories
			From 
			(
				Select v.volunteer_id, c.category_name 
				From volunteer v INNER JOIN VolunteerCategory vc ON v.volunteer_id = vc.volunteer_id 
				INNER JOIN Category c ON c.category_id = vc.category_id) t1
			) cat
		ON v.volunteer_id = cat.volunteer_id
	) as tbl
	Where volunteer_address like @PostCode
End

Go

Create Procedure sp_DoesRatingExist
@UserId int,
@VolunteerId int
AS
Begin
	Select * From VolunteerRating Where [user_id] = @UserId AND volunteer_id = @VolunteerId
End

Go

Create Procedure sp_AddRating
@UserId int,
@VolunteerId int,
@Rating int
AS
Begin
	Insert Into VolunteerRating([user_id], volunteer_id, rating) VALUES(@UserId, @VolunteerId, @Rating)
End

Go

Create Procedure sp_UpdateRating
@UserId int,
@VolunteerId int,
@Rating int
AS
Begin
	Update VolunteerRating Set rating = @Rating Where [user_id] = @UserId AND volunteer_id = @VolunteerId
End
